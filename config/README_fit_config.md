# How to use config scripts for beef.cc

Be aware to give sensible ranges to variables. *It is highly recommended to configure a the same range for observables in both configuration scripts!*

## Direct configurables:
- selection : ( `string`, default `""` )  Cut with wich the dataset will be read in (`TCut` format)

        selection "K_ProbNNk > 0.01 && D0BDT > -0.03 && LcBDT > -0.03"

- dsName : ( `string`, default `"Dred"` ) Name of `RooDataSet` with applied selection which will be saved in workspace of outputfile, default "Dred"
- pdf : ( `string`, ***mandatory*** ) Pdf to use in the fit. It has to be the name of the complete model given in `model`/RooFit factory settings
- suppress_RootInfo : (`bool`  , optional) Suppresses all ROOT info messages and all(?) RooFit messages (`RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING)`)
- weight : (`string`, optional) select weight variable for the RooDataSet
- weight_function : (`string`, optional) use weight variable in a function. for example: weight variable is efficiency, but we want the actual weight to be 1/efficiency

## Non-iteratable objects:
- model : (property tree child, ***mandatory***) List of factory commands to build the model pdf(s).

        model {
              Lbsignal         "Voigtian::gLb(mLb,mmLb[5620,5600,5650],wLb[4,0.1,10],sLb[8,2,20])"
              LbsignalDstg     "RooKeysPdf::LcDstgK"
              LbsignalDstpi0   "RooFFTConvPdf::LcDstpi0Kxg"
              LbsignalLcpr     "RooFFTConvPdf::LcprD0Kxg"
              LbsignalLcst     "RooFFTConvPdf::LcstD0Kxg"
              Background       "Exponential::bgLb(mLb,tau[-0.001,-0.1,0])"
              Model            "SUM::modelLb(NLb[1400,0,2000]*gLb,NbgLb[9000,0,100000]*bgLb,NLbDstg[1700,0,3000]*LcDstgK,NLbDstpi0[2400,0,4000]*LcDstpi0Kxg,NLbLcpr[300,0,600]*LcprD0Kxg,NLbLcst[100,0,500]*LcstD0Kxg)"
        }

- extended : (property tree child, triggers *SPlot*) Defines which parameters are treated as yield parameters by the *SPlot*

        extended {
            Nx
            NBg
        }

- fitoptions : (property tree child, optional) A set of arguments that can be used in `RooAbsPdf::fitTo` <br>
    The following parameters of are configurable
    - Extended        : (`bool`  , optional) Add extended likelihood term, off by default
    - SumW2Error      : (`bool`  , optional) Switch on uncertaintes for weighted datasets
    - InitialHesse    : (`bool`  , optional) Run HESSE before MIGRAD as well, off by default
    - Minos           : (`bool`  , optional) Flag controls if MINOS is run after HESSE, off by default
    - Verbose         : (`bool`  , optional) Flag controls if verbose output is printed
    - WarningsOff     : (`bool`  , optional) Disable MINUIT warnings (enabled by default)
    - Timer           : (`bool`  , optional) Time CPU and wall clock consumption of fit steps, off by default
    - Strategy        : (`int`   , optional) Set Minuit strategy (0 through 2, default is 1)
    - PrintLevel      : (`int`   , optional) Set Minuit print level (-1 through 3, default is 1)
    - PrintEvalErrors : (`int`   , optional) Control number of p.d.f evaluation errors printed per likelihood evaluation. A negative value suppress output completely, a zero value will only print the error count per p.d.f component, a positive value is will print details of each error up to numErr messages per p.d.f component.
    - MinimizerType   : (`string`, optional) Choose minimization package to use. Check [RooAbsPdf documentation](https://root.cern.ch/doc/master/classRooAbsPdf.html#a8f802a3a93467d5b7b089e3ccaec0fa8 "RooAbsPdf")
    - MinimizerAlg    : (`string`, optional) Choose minimization algorithm to use. *MinimizerType has to be be given!*
    - NumCPU          : (`int`   , optional) Parallelize NLL calculation on num CPUs
    - StratCPU        : (`int`   , optional) Parallelization strategy, see [RooAbsPdf documentation](https://root.cern.ch/doc/master/classRooAbsPdf.html#a8f802a3a93467d5b7b089e3ccaec0fa8 "RooAbsPdf") for details. *NumCPU has to be given!*
    - FitAsHist       : (`bool`  , optional) Fit binned version of the dataset. ***Important:*** To parse the binning, the identifier name of the plot must have the format *observable*plot (e.g. xplot, yplot for the 2 observables defined in the example above)

        fitoptions {
            NumCPU       8
            Extended     1
            Strategy     2
        }

- observables : (property tree child, mandatory if extended or FitAsHist is given) Defines which parameters are treated as observables by the *SPlot*

        observables {; for a 2D fit in x, y
            x
            y
        }

## Save fit results to .info file (optional)
- Enabled by adding the node `saveFitResultsAsInfo 1`
- Saves fit results (values and errors of all parameters) as a
  property tree to an .info file
- Filename is `output.info` with `output` from `-o
  <output.root>`

## Constrain parameters (optional)
- Enabled by adding the following node:

        constrainParams {
            mean1
            {
                value 5619.41
                error 0.04
            }
            sigma1
            {
                value 17.6
                error 0.6
        		}
        }

- `mean1` is the name of the parameter to constrain, given in the `model` part of the fitconfig file
- The `constrainParams` node can be created from an external fitresults file via the setParams tool (see corresponding README)

## Iterable objects (Optional):
- plots : Configures the plotting. All colors will be given as strings and parsed by `LHCb::color`, defined in `include/lhcbStyle.C` <br>
    All options below with ***no default*** value are mandatory as soon as the corresponding node is given in the config file <br>
    To make the list easier to read, *non-iterable childs* are italic, **iterable childs** are bold
    - **A list of iterable items** : They correspond to plots one wants to draw.<br>
      The identifier name in the property tree becomes important if a binned fit should be performed. The name must have the format *observable*plot then (e.g. xplot, yplot for the 2 observables defined in the example above).<br>
      The following parameters of each projection are configurable
        - var        : ( `string`, ***no default***     ) The name of the observable as it is in the workspace
        - low        : ( `double`, ***no default***     ) Lower limit of the x-axis
        - high       : ( `double`, ***no default***     ) High limit of the x-axis
        - bins       : ( `int`,    ***no default***     ) The number of bins with which the data is plotted
        - plotname   : ( `string`, default gets parsed  ) Name of output-graphic. Default is name of ouputfile + name of observable
        - xtitle     : ( `string`, default `"M"`        ) The title on the x-axis
        - ytitle     : ( `string`, default gets parsed  ) The title on the y-axis. Gets parsed dependent on the y-maximum and binsize, e.g. "10^{3} Events/4 MeV"
        - textsize   : ( `float`,  default `0.05`       ) Textsize for axis-labels and axis-title
        - CanvXSize  : ( `int`,    default `2048`       ) X size of canvas in pixel
        - CanvYSize  : ( `int`,    default `1280`       ) Y size of canvas in pixel
        - lmargin    : ( `float`,  default `0.11`       ) Left margin
        - tmargin    : ( `float`,  default `0.01`       ) Top margin
        - rmargin    : ( `float`,  default `0.01`       ) Right margin
        - bmargin    : ( `float`,  default `0.12`       ) Bottom margin (below the pulls)
        - pullheight : ( `float`,  default `0.32`       ) Height of the pullhistogram w.r.t. the plot of the fit. Note that the bottom margin will be subtracted from that
        - MSizeData  : ( `float`,  default `1.0`        ) Marker size data
        - MColorData : ( `string`, default `"black"`    ) Marker color data
        - LWidthData : ( `short`,  default `1`          ) Line width data
        - LColorData : ( `string`, default `"black"`    ) Line color data
        - OptionData : ( `string`, default `"e1p"`      ) Draw option data
        - DataError  : ( `int`,    default `3`          ) Error option for drawing. See https://root.cern.ch/doc/master/classRooAbsData.html#a04a1c5bab3785b72f0791d6966425d21 and https://root.cern.ch/doc/master/classRooAbsData.html#a3f1b47ae0cc4e217f514a8e4331f7e88
        - MSizePull  : ( `float`,  default `1.0`        ) Marker size pulls
        - MColorPull : ( `string`, default `"azure+3"`  ) Marker color pulls
        - LWidthPull : ( `short`,  default `1`          ) Line width pulls
        - LColorPull : ( `string`, default `"azure+3"`  ) Line color pulls
        - OptionPull : ( `string`, default `"e1p"`      ) Draw option pulls
        - xOffset    : ( `float`,  default `1.05`       ) X title offset
        - yOffset    : ( `float`,  default `1.2`        ) Y title offset
        - ymin       : ( `float`,  default `0.1`        ) Y range minimum (0.1 because with 0, the 0 label would be displayed and the pull pad would overlay this label)
        - ymax       : ( `float`,  default frame-max    ) Y range maximum
        - ymaxadd    : ( `float`,  default `0`          ) Adds `ymaxadd*(ymax-ymin)` to `ymax` (helpful to add space for big legends in a semi-automated way)
        - logy       : ( `bool`,   default `false`      ) Draw Y axis in log-scale (booleans are parsed from `int`'s)
        - xdiv       : ( `int`,    default `5`          ) Ndivisions of X axis
        - ytitlePull : ( `string`, default `"Pull "`    ) Y title of pull histogram
        - PullRange  : ( `float`,  default `4.8`        ) Pulls are drawn between `-PullRange` and `PullRange`
        - HideExpX   : ( `bool`,   default `false`      ) Hide 10^X on X axis which gets drawn automatically by `TGAxis`
        - HideExpY   : ( `bool`,   default `false`      ) Hide 10^X on Y axis which gets drawn automatically by `TGAxis`
        - MaxDigits  : ( `int` ,   default predefined   ) Set global maximum number of digits https://root.cern.ch/doc/master/classTGaxis.html#a6b93d66237560f7b11701402a1a446ce

        - *graphs*    : Draws projections of full model or its components.<br>
          Projections with fill style need to be drawn several times.<br>
          The order matters! And the `invisible` option helps if projections should be added.<br>
          Always plot full model at the end to get correct pulls (use invisible if projection shouldn't be drawn)
            - **A list of iterable items** : They correspond to projections one wants to draw.<br>
              The identifier name in the property tree is of no importance within the plotter. The following parameters of each projection are configurable
                - components    : ( `string`, ***no default*** ) Name of the component to draw. Given by pdfname in workspace (it's called above when setting up `model`/the factory commands)
                - linecolor     : ( `string`, default `"blue"` ) Linecolor of projection
                - linestyle     : ( `short` , default `1`      ) Linestyle of projection
                - linewidth     : ( `short`,  default `3`      ) Linewidth of projection
                - fillcolor     : ( `string`, default `"blue"` ) Fillcolor of projection
                - option        : ( `string`, default `"l"`    ) Draw option
                - addto         : ( `string`, default `""`     ) Name of projection this component should be added to. These are the names specified in components + "P". The P is internally added to avoid confusing `RooPlots` with pdfs/datasets.
                - invisible     : ( `int`,    default `0`      ) Add curve to frame, but do not display. Useful in combination addto
                - vlines        : ( `bool`,   default `false`  ) Add vertical lines to y=0 at end points of curve (if this doesn't work as expected, try `manual_vlines`)
                - manual_vlines : ( `bool`,   default `false`  ) Set last point of projection to 0 (the last point is usually just a duplicate of the previous)

        - *legends*   : Adds legends to frame. Legends are iterable, since one may want to have more than one legend, or a legend starting on the left and continuing on the right
            - **A list of iterable items** : They correspond to legends one wants to draw.<br>
              The identifier name in the property tree is of no importance within the plotter. The following parameters of each legend are configurable
                - header : ( `string`, default `""`  ) Header of the legend
                - xlo    : ( `double`, no default    ) X position in NDC coordinates
                - dtop   : ( `double`, no default    ) Distance from top margin to upper end of legend
                - height : ( `double`, no default    ) Height of legend in units of textsizes
                - dright : ( `double`, no default    ) Distance from right margin to right end of legend (text in legend can jut out)
                - scale  : ( `double`, default `1.0` ) Scalefactor of the legend's textsize w.r.t. the textsize given in `plot.textsize` (default `plot.textsize 0.05`)
                - **A list of iterable items**: They correspond to `RooPlot` clones one wants to refer to.<br>
                  The identifier name in the property tree is of no importance within the plotter. The following parameters of each `RooPlot` refence are configurable
                    - name  : ( `string`, no default     ) Name of the projection. These are the names of the pdf/dataset + "P". The P is internally added to avoid confusing `RooPlots` with pdfs/datasets.
                      More precise: Name of the pdf is given by `plot.graphs.<child>.components`. Name of the dataset is the name of the dataset in the workspace that contains `plot.var`
                    - label : ( `string`, no default     ) Label of component
                    - option: ( `double`, default `"lpf"` ) Distance from top margin to upper end of legend

        - *pullLines* : Adds lines to pull frame.
            - **A list of iterable items** : They correspond to lines which one may want to configure in different styles.
              The identifier name of the line in the pt is of no importance within the plotter. The following parameters are configurable
                - pull   : ( `double`, no default        ) Y position of the line
                - style  : ( `short`,  default `2`       ) Line style
                - width  : ( `short`,  default `1`       ) Line width
                - color  : ( `string`, default `"black"` ) Line color
                - option : ( `string`, default `"l"`     ) Draw option
        - *outputformats*
            - A list of `string`s. Their names correspond to parsable output formats of [TCanvas::SaveAs](https://root.cern.ch/doc/master/classTCanvas.html#abb7a40ea658c348cdc8f6925eb671314 "TCanvas::SaveAs")

        Full blown plotting example from Lb->LcD0-barK

            plot {
                plotname "Lbfit"
                var      "mLb"
                xtitle   "M_{inv} (#Lambda^{+}_{c}#bar{D}^{0} K^{-} + c.c.) (MeV)"
                low      5240
                high     5840
                bins     150
                HideExpX 1   ;bools are parsed from 0 or 1
                xOffset  0.9
                yOffset  0.95
                lmargin  0.087
                bmargin  0.104
                rmargin  0.004

                graphs {
                    background {
                        components "bgLb"
                        invisible  1
                    }
                    LbpartDstg {
                        components "LcDstgK"
                        addto      "bgLbP"
                        invisible  1
                    }
                    LbpartDstpi0 {
                        components "LcDstpi0Kxg"
                        option     "F"
                        fillcolor  "spring"
                        linecolor  "spring"
                        linewidth  0
                        addto      "LcDstgKP"
                    }
                    LbpartLcst {
                        components "LcstD0Kxg"
                        addto      "LcDstgKP"
                        invisible  1
                    }
                    LbpartLcpr {
                        components "LcprD0Kxg"
                        option     "F"
                        fillcolor  "orange+2"
                        linecolor  "orange+2"
                        linewidth  0
                        addto      "LcstD0KxgP"
                    }
                    LbpartLcstpersistent {
                        components "LcstD0Kxg"
                        option     "F"
                        fillcolor  "orange"
                        linecolor  "orange"
                        linewidth  0
                        addto      "LcDstgKP"
                    }
                    LbpartDstgpersistent {
                        components "LcDstgK"
                        option     "F"
                        fillcolor  "green+3"
                        linecolor  "green+3"
                        linewidth  0
                        addto      "bgLbP"
                    }
                    backgroundpersistent {
                        components "bgLb"
                        fillcolor  "gray+1"
                        linecolor  "gray+1"
                        linewidth  0
                        option     "F"
                    }
                    signal {
                        components "gLb"
                        linecolor  "red"
                        linewidth  2
                        style      1
                    }
                    full {
                        components "modelLb"
                        linecolor  "blue"
                        linewidth  2
                        style      1
                    }
                }

                legends {
                    leg1 {
                        header "LHCb internal"
                        xlo    0.7
                        dtop   0.03
                        height 9.0
                        dright 0.03
                        scale  0.65
                        signal {
                            name   "gLbP"
                            label  "#Lambda_{b}^{0}#rightarrow #Lambda_{c}^{+}#bar{D}^{0}K^{-}"
                            option "l"
                        }
                        background {
                            name   "bgLbP"
                            label  "comb. bkg"
                            option "f"
                        }
                        LbpartDstg {
                            name   "LcDstgKP"
                            label  "#Lambda_{b}^{0}#rightarrow#Lambda_{c}^{+}#left[#bar{D}^{0}#color[2]{#gamma}#right]_{#bar{D}*(2007)^{0}}K^{-}"
                            option "f"
                        }
                        LbpartDstpi0 {
                            name   "LcDstpi0KxgP"
                            label  "#Lambda_{b}^{0}#rightarrow#Lambda_{c}^{+}#left[#bar{D}^{0}#color[2]{#pi^{0}}#right]_{#bar{D}*(2007)^{0}}K^{-}"
                            option "f"
                        }
                        LbpartLcpr {
                            name   "LcprD0KxgP"
                            label  "#Lambda_{b}^{0}#rightarrow#left[#Lambda_{c}^{+}#color[2]{#pi^{-}#pi^{+}}#right]_{#Lambda_{c}(2595)^{+}}#bar{D}^{0}K^{-}"
                            option "f"
                        }
                        LbpartLcst {
                            name   "LcstD0KxgP"
                            label  "#Lambda_{b}^{0}#rightarrow#left[#Lambda_{c}^{+}#color[2]{#pi^{-}#pi^{+}}#right]_{#Lambda_{c}(2625)^{+}}#bar{D}^{0}K^{-}"
                            option "f"
                        }
                    }
                }

                pullLines {
                    l1 {
                      pull 0
                    }
                    l2 {
                      pull -3
                    }
                    l3 {
                      pull 3
                    }
                }

                outputformats {
                    pdf
                    png
                    C
                    tex
                }
            }
