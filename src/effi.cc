//from std
#include <string>
#include <iostream>
#include <vector>
#include <functional>
//from BOOST
#include <boost/algorithm/string/replace.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string/predicate.hpp>
//from ROOT
#include "Math/PdfFuncMathCore.h"
#include "TError.h"
#include "TFile.h"
#include "TMath.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TString.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TTree.h"

#include "TLine.h"
#include "TLegend.h"
//from ROOFIT
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooDataSet.h"
#include "RooConstVar.h"
#include "RooFormulaVar.h"
#include "RooFitResult.h"
#include "RooCmdArg.h"
#include "RooLinkedList.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooMsgService.h"
#include "RooPoisson.h"
#include "RooTFnBinding.h"
#include "RooCFunction3Binding.h"

#include "RooPlot.h"
#include "RooMinuit.h"
#include "RooMinimizer.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/BayesianCalculator.h"
//local
#include "../include/plotter.h"
#include "../IOjuggler/IOjuggler.h"

#ifdef BEEF_GIT_HASH
#define BEEF_HASH BEEF_GIT_HASH
#else
#define BEEF_HASH " "
#endif

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner(){return 0;}

int main(int argc, char** argv){

  int effi_status = 0;

  TStopwatch clock;
  clock.Start();

  //get some stuff, IOjuggler takes care of this
  //parse options pass to executable
  auto options = parse_options(argc, argv, "c:d:hi:o:r:v:w:","nonoptions: cutstring",1);
  MessageService msgsvc("effi",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.infomsg("Current beef git hash: " + static_cast<std::string>(BEEF_HASH));
  //open file, check for absolute path and a path with prepended workdir
  auto infile = get_file(options.get<std::string>("infilename"),options.get<std::string>("workdir"));
  auto w = get_obj<RooWorkspace>(infile,options.get<std::string>("wsname"));
  //get ptree
  pt::ptree configtree = get_ptree(options.get<std::string>("config"));
  //get cutstring from nonoptions
  const auto cut = options.get_child("extraargs").front().second.data();

  //append and replace stuff in ptree
  auto_append_in_ptree(configtree);
  auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    print_ptree(configtree);

  if(configtree.get_optional<bool>("suppress_RootInfo")){
    gErrorIgnoreLevel = kWarning;
    RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
  }

  // get dataset
  auto dsFull = get_wsobj<RooDataSet>(w,options.get<std::string>("dsname"));
  if(configtree.get_optional<std::string>("weight")){
    if(!configtree.get_optional<bool>("fitoptions.SumW2Error")){
      msgsvc.warningmsg("You are working with a weighted dataset, but did not make use of the SumW2Error option.");
      msgsvc.warningmsg("The uncertainties returned by the fit will only be correct if your weights are properly normalised!");
    }
    if(configtree.get_optional<std::string>("weight_function")){
      auto weight_var = get_wsobj<RooRealVar>(w,configtree.get<std::string>("weight"));
      RooFormulaVar wFunc("wfunc","event weight",
                          configtree.get<std::string>("weight_function").data(),*weight_var);
      auto addedwf = static_cast<RooRealVar*>(dsFull->addColumn(wFunc));
      dsFull = new RooDataSet(dsFull->GetName(),dsFull->GetTitle(),dsFull,*dsFull->get(),"",
                              addedwf->GetName());
    }
    else
      dsFull = new RooDataSet(dsFull->GetName(),dsFull->GetTitle(),dsFull,*dsFull->get(),"",
                              configtree.get<std::string>("weight").data());
  }
  msgsvc.infomsg(TString::Format("%.0f candidates in dataset", dsFull->sumEntries()));

  // apply cuts
  RooDataSet* ds_total = nullptr;
  if(auto selection = configtree.get_optional<std::string>("selection")){
    msgsvc.infomsg("Selection: " + *selection);
    ds_total = static_cast<RooDataSet*>(dsFull->reduce(
                                          RooFit::Cut((*selection).data()),
                                          RooFit::Name(configtree.get("DsTotal","ds_total").data())));
  }
  else
    ds_total = new RooDataSet(*dsFull,configtree.get("DsTotal","ds_total").data());
  const auto ntotal_sb = ds_total->sumEntries();
  msgsvc.infomsg(TString::Format("%.0f candidates selected (Total)", ntotal_sb));

  //make pass set from nonoption-cut
  auto ds_pass = static_cast<RooDataSet*>(ds_total->reduce(
                                            RooFit::Cut(cut.data()),
                                            RooFit::Name(configtree.get("DsPass","ds_pass").data())));
  const auto npass_sb = ds_pass->sumEntries();
  msgsvc.infomsg(TString::Format("%.0f candidates selected (Pass)", npass_sb));
  w->import(*ds_total);
  w->import(*ds_pass);

  //get observables
  RooArgSet observables;
  if(!configtree.get_child_optional("observables"))
    throw std::runtime_error("\"observables\" not found in config file");
  for(const auto& obs : configtree.get_child("observables")){
    auto myobs = get_wsobj<RooRealVar>(w,obs.first);
    myobs->setBins(configtree.get<int>(("plots."+obs.first+"plot.bins").data(),100));
    observables.add(*myobs);
  }

  // Build model from factory strings in fitconfig file
  auto build_model = [&w,&msgsvc] (const auto& model_name) {
    if(!model_name)
      throw std::runtime_error("a required model has not been found in the config file");
    msgsvc.debugmsg("Model: ");
    for(const auto &v : *model_name) {
      const auto component = v.second.data();
      msgsvc.debugmsg("      " + component);
      w->factory(component.data());
    }
  };

  build_model(configtree.get_child_optional("model_total"));
  build_model(configtree.get_child_optional("model_pass"));

  //extend the model to a simultaneous one
  //some sanity checks of the config file
  const auto model_name  = configtree.get_optional<std::string>("pdf");
  const auto nsig_name   = configtree.get_optional<std::string>("NSigName");
  const auto nbkg_name   = configtree.get_optional<std::string>("NBkgName");
  const auto sigpdf_name = configtree.get_optional<std::string>("SigPdfName");
  const auto bkgpdf_name = configtree.get_optional<std::string>("BkgPdfName");
  if(!model_name) throw std::runtime_error("\"pdf\" not found in config file");
  if(!nsig_name || !nbkg_name || !sigpdf_name || !bkgpdf_name)
    throw std::runtime_error("\"NSigName\", \"NbkgName\", \"SigPdfName\" or \"BkgPdfName\" not found in config file");

  //get total pdf
  auto totalPdf = get_wsobj<RooAbsPdf>(w,*model_name+"_total");

  //get fitoptions
  RooLinkedList llist;
  std::vector<RooCmdArg*> args;
  RooArgSet parameters_for_minos;
  auto set_fitoptions = [&] (const std::string&& category) {
    const auto node_name = "fitoptions_" + category;
    args.push_back( new RooCmdArg( RooFit::Save() ) );//we want to save the fitresult, so this is not optional
    if(configtree.get_optional<bool>(node_name+".Extended"))
      args.push_back( new RooCmdArg( RooFit::Extended()) );
    if(configtree.get_optional<bool>(node_name+".SumW2Error"))
      args.push_back( new RooCmdArg( RooFit::SumW2Error(true)) );
    if(configtree.get_optional<bool>(node_name+".InitialHesse"))
      args.push_back( new RooCmdArg( RooFit::InitialHesse()) );
    if(auto pois = configtree.get_child_optional(node_name+".Minos")){
      for(const auto& poi : *pois){
        parameters_for_minos.add(*get_wsobj<RooAbsArg>(w,poi.first));
        msgsvc.infomsg("Adding " + poi.first + " to MINOS arguments");
      }
      args.push_back( new RooCmdArg( RooFit::Minos(parameters_for_minos)) );
    }
    else if(configtree.get_optional<bool>(node_name+".Minos"))
      args.push_back( new RooCmdArg( RooFit::Minos()) );
    if(configtree.get_optional<bool>(node_name+".Verbose"))
      args.push_back( new RooCmdArg( RooFit::Verbose()) );
    if(configtree.get_optional<bool>(node_name+".WarningsOff"))
      args.push_back( new RooCmdArg( RooFit::Warnings(false)) );
    if(configtree.get_optional<bool>(node_name+".Timer"))
      args.push_back( new RooCmdArg( RooFit::Timer()) );
    if(auto opt = configtree.get_optional<int> (node_name+".Strategy"))
      args.push_back( new RooCmdArg( RooFit::Strategy(*opt) ) );
    if(auto opt = configtree.get_optional<int> (node_name+".PrintLevel"))
      args.push_back( new RooCmdArg( RooFit::PrintLevel(*opt) ) );
    if(auto opt = configtree.get_optional<int> (node_name+".PrintEvalErrors"))
      args.push_back( new RooCmdArg( RooFit::PrintEvalErrors(*opt) ) );
    if(auto opt = configtree.get_optional<std::string>(node_name+".MinimizerType"))
      args.push_back( new RooCmdArg( RooFit::Minimizer( (*opt).data(), configtree.get(node_name+".MinimizerAlg","migrad").data()) ) );
    if(auto opt = configtree.get_optional<int>(node_name+".NumCPU"))
      args.push_back( new RooCmdArg( RooFit::NumCPU(*opt,configtree.get(node_name+".StratCPU",0)) ) );

    for (auto arg : args){
      //the next line forbids usage of RooCmdArgs as r-value,
      //also taking the adress of a reference and casting to TObject* can't be handled by RooFit, thus the pointers
      arg->setProcessRecArgs(true,false);
      llist.Add(arg);
    }

  };

  set_fitoptions("total");

  //fitting-lambda
  auto do_fit = [&configtree,&msgsvc,&observables] (RooDataSet* ds, RooAbsPdf* pdf,
                RooLinkedList& llist, const std::string&& name) {
    if(configtree.get_optional<bool>("fitoptions_"+name+".FitAsHist")){
      msgsvc.infomsg("Converting dataset to histogram and performing a binned fit");
      RooDataHist hist("hist","hist",observables,*ds);
      return pdf->fitTo(hist,llist);
    }
    else
      return pdf->fitTo(*ds,llist);
  };

  auto eval_fit_result = [&msgsvc,&effi_status] (RooFitResult* fr, const std::string&& name){
    const auto edm = fr->edm();
    const auto fs = fr->status();
    const auto cq = fr->covQual();
    if(edm > 0.001) msgsvc.warningmsg(name + " fit has large estimated distance to minimum (edm): "+std::to_string(edm));
    if(fs != 0){
      effi_status += fs;
      msgsvc.warningmsg(name + " fit has bad fit status: "+std::to_string(fs));
    }
    if(cq != 3) msgsvc.warningmsg(name + " fit has bad covariance matrix: "+std::to_string(cq));
    return fr;
  };

  //fit total with floating parameters
  const auto total_fitresult = eval_fit_result(do_fit(ds_total,totalPdf,llist,"total"),"Floating total");

  if(effi_status > 0){
    msgsvc.errormsg("Bad total fit. Aborting...");
    return effi_status;
  }

  //delete pointers from heap
  auto cleanup_after_fit = [&args,&llist,&parameters_for_minos] () {
    for (auto arg : args)
      delete arg;
    args.clear();
    llist.Clear();
    parameters_for_minos.Clear();
  };
  cleanup_after_fit();

  //preparations for fitting pass data
  //i'm not sure if the conditional observable change anything (maybe they are set automatically if you use external constraints)
  //still leaving them in since it makes sense in my understanding
  RooArgSet constrainFuncs, conditional_observables;

  //fix number of total signal an background events now, but build constraining Poisson pdf to transport them to the pass fit
  w->var((*nsig_name+"_total").data())->setConstant();
  w->var((*nbkg_name+"_total").data())->setConstant();

  //put pass fit together, use fixed N_total*efficiency as RooFormulaVar. So the efficiency becomes a fit-parameter
  w->factory(("SUM::pass_pdf(expr('@0*@1',{eff_sig["+std::to_string(npass_sb/ntotal_sb)+",0.0,1.0],"+*nsig_name+"_total})*"+*sigpdf_name+"_pass,"
                            "expr('@0*@1',{eff_bkg["+std::to_string(npass_sb/ntotal_sb)+",0.0,1.0],"+*nbkg_name+"_total})*"+*bkgpdf_name+"_pass)").data());

  //optional binomial constraint (likelihood shape is different when switching this on)
  if(configtree.get_optional<bool>("binomial_constraint")){
    auto bin_sig = RooFit::bindPdf("binomial_sig",ROOT::Math::binomial_pdf,*w->function("pass_pdf_1"),*w->var("eff_sig"),*w->var((*nsig_name+"_total").data()));
    auto bin_bkg = RooFit::bindPdf("binomial_bkg",ROOT::Math::binomial_pdf,*w->function("pass_pdf_2"),*w->var("eff_bkg"),*w->var((*nbkg_name+"_total").data()));
    w->import(*bin_sig);
    w->import(*bin_bkg);
    constrainFuncs.add(*w->pdf("binomial_sig"));
    constrainFuncs.add(*w->pdf("binomial_bkg"));
    conditional_observables.add(*w->var("eff_sig"));
    conditional_observables.add(*w->var("eff_bkg"));
  }
  //optional beta constraint (the beta distribution is the conjugate prior probability distribution
  //for the Bernoulli, binomial, negative binomial and geometric distributions in Bayesian statistics)
  //switch on, if the efficiencies are close to 0 or 1 to make fits more stable, but check for bias if switched on.
  //try with beta_par = 1 to 1.5 first, go to higher values if needed (but then really do toys to check for biases!)
  auto do_beta_constraint = [&w,&configtree,&constrainFuncs,&conditional_observables] () {
    w->factory(("beta_par["+std::to_string(configtree.get("beta_par",1.0))+"]").data());
    auto beta_sig = RooFit::bindPdf("beta_sig",ROOT::Math::beta_pdf,*w->var("eff_sig"),*w->var("beta_par"),*w->var("beta_par"));
    auto beta_bkg = RooFit::bindPdf("beta_bkg",ROOT::Math::beta_pdf,*w->var("eff_bkg"),*w->var("beta_par"),*w->var("beta_par"));
    w->import(*beta_sig);
    w->import(*beta_bkg);
    constrainFuncs.add(*w->pdf("beta_sig"));
    constrainFuncs.add(*w->pdf("beta_bkg"));
    if(conditional_observables.getSize() < 2){
      conditional_observables.add(*w->var("eff_sig"));
      conditional_observables.add(*w->var("eff_bkg"));
    }
  };
  if(configtree.get_optional<bool>("beta_constraint"))
    do_beta_constraint();

  //Gaussian constraints to nuisance parameters. width of Gaussian can be coupled to efficiency
  //iterate sig and bkg shape-parameters and constrain their values to a Gaussian with mean from total fit and
  //width given by "sig_constr" expression.
  //Initially, a constraint on the correlation between total and pass was planned, but this should be sufficient
  if(configtree.get_optional<bool>("constrain_nuisance_parameters")){

    //widths of Gaussian
    const auto sig_width = configtree.get("sig_constr","expr('PAR_ERR*(1.0-@0)',eff_sig)");
    const auto bkg_width = configtree.get("bkg_constr","expr('PAR_ERR*(1.0-@0)',eff_bkg)");

    //functor that determines wheter to constrain a parameter
    std::function<bool(const std::string&)> parskipper;
    boost::optional<pt::ptree> bla;
    if(bla = configtree.get_child_optional("nuisance_parameters_to_constrain")){
      msgsvc.debugmsg("Using a user-provided list of parameters to constrain");
      parskipper = [&bla] (const std::string& parname) {
        for(const auto& cpn : *bla)
          if(boost::algorithm::starts_with(parname,cpn.first))
            return false;
        return true;
      };
    }
    else if(bla = configtree.get_child_optional("exclude_nuisance_parameters")){
      msgsvc.debugmsg("Skipping constraining of user-provided nuisance parameters");
      parskipper = [&bla] (const std::string& parname) {
        for(const auto& excl : *bla)
          if(boost::algorithm::starts_with(parname,excl.first))
            return true;
        return false;
      };
    }
    else parskipper = [] (const std::string&) {return false;};

    auto iterate_and_constrain = [&parskipper,&msgsvc,&w,&constrainFuncs,&conditional_observables]
                                 (auto&& iterator, const auto width) {
      while(auto pars = static_cast<RooRealVar*>(iterator->Next())){
        pars->setConstant();
        std::string tname = pars->GetName();
        //set value of total fit
        const auto pname = boost::algorithm::replace_all_copy(tname,"_total","_pass");
        w->var(pname.data())->setVal(pars->getVal());
        if(parskipper(tname)) continue;
        if(msgsvc.GetMsgLvl()>=MSG_LVL::DEBUG){
          msgsvc.debugmsg("Constraining var");
          w->var(tname.data())->Print();
          w->var(pname.data())->Print();
        }
        w->factory(("Gaussian::constr_"+pname+"("
                    +pname+","//constrain pass-parameter
                    +tname+","//with total-parameter as mean, and sig_width as width:
                    +boost::algorithm::replace_all_copy(width,"PAR_ERR",std::to_string(pars->getError()))
                    +")").data());
        constrainFuncs.add(*get_wsobj<RooAbsArg>(w,"constr_"+pname));
        conditional_observables.add(*w->var(pname.data()));
      }
    };
    iterate_and_constrain(get_wsobj<RooAbsPdf>(w,*sigpdf_name+"_total")->getParameters(*ds_total)
                          ->selectByAttrib("Constant",false)->createIterator(),sig_width);
    //create the iterator here, so that parameters that have been used in the signal pdf
    //are fixed already and we don't get handling warnings
    iterate_and_constrain(get_wsobj<RooAbsPdf>(w,*bkgpdf_name+"_total")->getParameters(*ds_total)
                          ->selectByAttrib("Constant",false)->createIterator(),bkg_width);
  }
  args.push_back( new RooCmdArg(RooFit::ExternalConstraints(constrainFuncs)));
  args.push_back( new RooCmdArg(RooFit::ConditionalObservables(conditional_observables)));

  set_fitoptions("pass");

  auto passPdf = w->pdf("pass_pdf");
  auto pass_fitresult = eval_fit_result(do_fit(ds_pass,passPdf,llist,"pass"),"Floating pass");
  cleanup_after_fit();

  if(effi_status > 0 && configtree.get_optional<bool>("pray")){
    //The problem is, that the estimated efficiency tends towards the limits for very high (or low) true efficiencies.
    //try to penalise this behaviour by a beta constraint
    msgsvc.warningmsg("Trying to pick up the pieces. Keep your fingers crossed");
    const auto Nst_val = w->var((*nsig_name+"_total").data())->getVal();
    const auto tmp_effval = w->var("eff_sig")->getVal();
    const auto pseudo_rel_efficiency_error = sqrt(tmp_effval*Nst_val)/(tmp_effval*Nst_val);

    if(tmp_effval < 4*pseudo_rel_efficiency_error || tmp_effval > 1.0 - 4*pseudo_rel_efficiency_error){

      //lambda to remove constraints from previous beta pdfs
      auto remove_constraints = [&constrainFuncs,&w] () {
        constrainFuncs.remove(*w->pdf("beta_sig"));
        constrainFuncs.remove(*w->pdf("beta_bkg"));
      };

      //check if there was already a constraint
      if(w->pdf("beta_sig") == nullptr)
        do_beta_constraint();
      else
        remove_constraints();

      //try to fit again with (ever stronger) constraints
      auto previous_effi_status = effi_status, delta_effi_status = effi_status;
      while(const auto beta_par_val = w->var("beta_par")->getVal() < 2.4 && delta_effi_status > 0){
        msgsvc.infomsg(TString::Format("Fitting with beta constraint. Setting alpha and beta to %.1f",beta_par_val+0.2));
        w->var("beta_par")->setVal(beta_par_val+0.2);//yes, we start with min. 1.2
        constrainFuncs.add(*w->pdf("beta_sig"));
        constrainFuncs.add(*w->pdf("beta_bkg"));
        args.push_back( new RooCmdArg(RooFit::ExternalConstraints(constrainFuncs)));
        args.push_back( new RooCmdArg(RooFit::ConditionalObservables(conditional_observables)));
        set_fitoptions("pass");
        pass_fitresult = eval_fit_result(do_fit(ds_pass,passPdf,llist,"pass"),"beta constrained pass");
        cleanup_after_fit();
        remove_constraints();
        delta_effi_status = effi_status - previous_effi_status;
        if(delta_effi_status == 0) effi_status = 0;
      }
    }
    else
      msgsvc.errormsg("The bad fit quality is not due to extreme efficiencies. Can't do anything about it...");
  }

  const double effval   = w->var("eff_sig")->getVal();
  const double efferrlo = w->var("eff_sig")->getErrorLo();
  const double efferrhi = w->var("eff_sig")->getErrorHi();
  msgsvc.infomsg(TString::Format("Fitted signal efficiency : %.3f %.3f + %.3f %%",100*effval,100*efferrlo,100*efferrhi));

  const auto outfilename = options.get<std::string>("workdir") + "/" + options.get<std::string>("outfilename");

  pt::ptree ptree_fitres;
  ptree_fitres.add("sig_eff.Value",effval);
  ptree_fitres.add("sig_eff.ErrorHi",efferrhi);
  ptree_fitres.add("sig_eff.ErrorLo",efferrlo);

  if (configtree.get("saveFitResultsAsInfo",0)) {
    auto tit = w->pdf((*model_name+"_total").data())->getVariables()->createIterator();
    while (const auto vt = static_cast<RooRealVar*>(tit->Next())){
      std::string varname = vt->GetName();
      ptree_fitres.add(varname + ".Value",vt->getVal());
      ptree_fitres.add(varname + ".Error",vt->getError());
    }
    auto pit = passPdf->getVariables()->createIterator();
    while (const auto vp = static_cast<RooRealVar*>(pit->Next())){
      std::string varname = vp->GetName();
      ptree_fitres.add(varname + ".Value",vp->getVal());
      ptree_fitres.add(varname + ".Error",vp->getError());
    }
  }
  pt::write_info(boost::algorithm::replace_all_copy(outfilename,".root",".info"),ptree_fitres);

  infile->Close();
  auto outfile = TFile::Open(outfilename.data(),"RECREATE");
  outfile->cd();
  //create a new workspace to avoid seg-faults arising from cached pdfs like
  //RooFFTConvPdf when trying to access the old workspace in the new file
  RooWorkspace ws("w",true);
  ws.import(*ds_total);
  ws.import(*ds_pass);
  ws.import(*pass_fitresult);
  ws.import(*total_fitresult);
  ws.import(*totalPdf);
  ws.import(*passPdf);
  ws.Write();

  // create control plot
  if(const auto plotnode = configtree.get_child_optional("plots_total"))
    for(const auto& plot : *plotnode)
      plotter(ws,ds_total,totalPdf,outfile,options.get<std::string>("workdir"),options.get<std::string>("prefix",""),plot.second,msgsvc);
  if(const auto plotnode = configtree.get_child_optional("plots_pass"))
    for(const auto& plot : *plotnode)
      plotter(ws,ds_pass,passPdf,outfile,options.get<std::string>("workdir"),options.get<std::string>("prefix",""),plot.second,msgsvc);

  outfile->Delete("*");
  outfile->Close("R");

  clock.Stop();
  if(!configtree.get_optional<bool>("suppress_RootInfo"))
    clock.Print();

  return effi_status;

}
