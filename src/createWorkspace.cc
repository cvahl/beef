//ROOT
#include "TROOT.h"
#include "TString.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TFriendElement.h"
//ROOFIT
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooKeysPdf.h"
#include "RooGaussian.h"
#include "RooFFTConvPdf.h"
//C++
#include <string>
#include <vector>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>
//IOjuggler
#include "../IOjuggler/IOjuggler.h"

#ifdef BEEF_GIT_HASH
  #define BEEF_HASH BEEF_GIT_HASH
#else
  #define BEEF_HASH " "
#endif

namespace pt = boost::property_tree;

int main(int argc, char** argv){

  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  auto options = parse_options(argc, argv, "c:d:i:o:t:hv:","nonoptions: any number of <file:friendtree> combinations");
  MessageService msgsvc("createWorkspace",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.infomsg("Current beef git hash: " + static_cast<std::string>(BEEF_HASH));
  //open file, check for absolute path and a path with prepended workdir
  auto infile = get_file(options.get<std::string>("infilename"),options.get<std::string>("workdir"));
  auto tree = get_obj<TTree>(infile,options.get<std::string>("treename"));
  //get ptree
  pt::ptree configtree = get_ptree(options.get<std::string>("config"));

  if(configtree.get_optional<bool>("suppress_RootInfo")){
    gErrorIgnoreLevel = kWarning;
    RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
  }

  //append and replace stuff in ptree
  auto_append_in_ptree(configtree);
  auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    print_ptree(configtree);

  //add friend tree(s). for this we have to fiddle apart file and friendtree in the nonoptions, i.e. <file:friendtree>
  //and because they will go out of scope, we have to push them into a vector
  std::vector< TFriendElement* > fes;
  if(options.get_child_optional("extraargs")){
    for(const auto& ea : options.get_child("extraargs")){
      auto ff = get_file(static_cast<std::string>(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(0)))->String().Data()),options.get<std::string>("workdir"));
      fes.push_back(tree->AddFriend(static_cast<TObjString *>((static_cast<TString>(ea.second.data()).Tokenize(":")->At(1)))->String().Data(),ff));
    }
  }
  
  tree->SetBranchStatus("*",false);

  std::vector<RooRealVar> variables;
  variables.reserve(configtree.get_child("variables").size());
  RooArgSet vars("variables");
  //RooCmdArg importCommands("MultiArg",0,0,0,0,0,0,0,0);
  //importCommands.setProcessRecArgs(true);

  //prepare observable
  auto obs_name = configtree.get<std::string>("observable.name");
  auto obs_low  = configtree.get<double>("observable.low");
  auto obs_high = configtree.get<double>("observable.high");
  RooRealVar Observable(obs_name.c_str(),obs_name.c_str(),obs_low,obs_high);
  //variables.push_back(Observable);
  //vars.add(variables.back());
  vars.add(Observable);
  boost::replace_all(obs_name, "[0]", "");
  //declare variable to check if the branch is there (a bit strangely implemented in ROOT)
  unsigned int found = 99999;//this works unless you set the status of 99999 branches at once :D
  tree->SetBranchStatus(obs_name.c_str(),true,&found);//found is set to the number of branches matching obs_name
  if(found == 99999) throw std::runtime_error("Branch "+obs_name+" not found");

  //add "spectator" variables
  if(configtree.get_child_optional("variables")){
    for(const auto& v : configtree.get_child("variables")){
      // The data function is used to access the data stored in a node.
      std::string name=v.first;
      double low=v.second.get<double>("low");
      double high=v.second.get<double>("high");

      // check if there is an override for this variable
      std::string overloc="variables_override";
      if(configtree.find(overloc)!=configtree.not_found())
      {
        pt::ptree& overrides=configtree.get_child(overloc);
        if(overrides.find(name)!=overrides.not_found())
        {
          pt::ptree& override=overrides.get_child(name);
          msgsvc.infomsg(TString::Format("overriding range for %s", name.data()));
          low=override.get<double>("low");
          high=override.get<double>("high");
        }
      }

      RooRealVar myvar(name.c_str(),name.c_str(),low,high);
      variables.push_back(myvar);
      // here we refer to an entry in a vector
      // potentially dangerous if the vector is changed afterwards
      vars.add(variables.back());
      // for renaming the branches: DOES NOT WORK BECAUSE ROOSHIT
      //importCommands.addArg(RenameVariable(branch.c_str(),name.c_str()));

      // switch on the branches we need:
      boost::replace_all(name, "[0]", "");
      found = 99999;//reset it
      tree->SetBranchStatus(name.c_str(),true,&found);
      if(found == 99999) throw std::runtime_error("Branch "+name+" not found");
    }
  }

  msgsvc.debugmsg("Loading data ...");
  auto dsname = configtree.get("dsname","ds");
  RooDataSet ds(dsname.data(),dsname.data(),vars,RooFit::Import(*tree),RooFit::Cut(configtree.get("basiccuts","").c_str())) ;

  if(ds.numEntries()==0) {
    msgsvc.warningmsg("0 events selected. Aborting.");
    return 1;
  }
  else msgsvc.infomsg(TString::Format("Imported %d events.",ds.numEntries()));

  infile->Close();

  RooWorkspace w("w",true);
  w.import(ds); //,importCommands,RenameVariable("lab0_Cons_M[0]","mLb"));

  //prepare RooKeysPdfs (optional)
  if(configtree.get_child_optional("kdepdfs")){
    for(const auto& v : configtree.get_child("kdepdfs")){
      //get shapes from XFeed MC files - access the file and tree
      auto MCf = get_file(v.second.get<std::string>("filename"),options.get<std::string>("workdir"));
      auto MCt = get_obj<TTree>(MCf,v.second.get<std::string>("treepath"));
      //load data from tree manually, since using import is in general not possible.
      //This is because the variable in MCtree should have the same name as the fit-observable
      auto MCl = MCt->GetLeaf( static_cast<std::string>(v.second.get("obsinfile",Observable.GetName())).c_str() );
      RooDataSet MCds("MCds","MCds",Observable);
      auto nEntriesMC = MCt->GetEntries();
      //for the resolution function's <Q> (see below)
      auto meanQ = 0.f; unsigned int acc = 0u;
      for (decltype(nEntriesMC) MCev = 0; MCev < nEntriesMC; MCev++){
        MCt->GetEntry(MCev);
        Observable.setVal(MCl->GetValue(0));
        if(obs_low < Observable.getVal() && Observable.getVal() < obs_high){
          MCds.add(Observable);
          meanQ += Observable.getVal();
          acc++;
        }
      }

      //make KDE-pdf from MC data
      RooKeysPdf kdepdf(v.first.c_str(),v.first.c_str(),Observable,MCds,
                        static_cast<RooKeysPdf::Mirror>(v.second.get("mirror",3)),
                        static_cast<double>(v.second.get("rho",1.f))
                        );

      //fold it with the detector resolution (optional)
      //it's not possible to directly fold a resolution function with mass dependent width https://root.cern.ch/phpBB3/viewtopic.php?t=14482
      //we keep the resolution fixed at sqrt(<Q>), where <Q> is the energy release of the sample mean of our keys pdf
      if (configtree.get_child_optional("resolution") && static_cast<bool>(v.second.get("fold",1))){
        meanQ /= static_cast<decltype(meanQ)>(acc);
        meanQ -= configtree.get("resolution.MassThreshold",0.f);
        auto resolution = sqrt(meanQ)*configtree.get("resolution.ResolutionScaling",1.f);
        msgsvc.infomsg(TString::Format("Folding %s with a %.2f MeV Gaussian resolution",kdepdf.GetName(),resolution));
        RooRealVar mg(TString::Format("%smg",kdepdf.GetName()).Data(),"mg",0);
        RooRealVar sg(TString::Format("%ssg",kdepdf.GetName()).Data(),"sg",resolution);
        RooGaussian gauss(TString::Format("%sres",kdepdf.GetName()).Data(),"Gaussian resolution",Observable,mg,sg);
        RooFFTConvPdf kxg(TString::Format("%sxg",kdepdf.GetName()).Data(),"kde (X) gauss",Observable,kdepdf,gauss);
        //w makes copy when importing. so we are safe when importing it in this scope and writing it later to file
        w.import(kxg);
      }
      else
        w.import(kdepdf);
      MCf->Close();
    }
  }

  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    w.Print();
  w.writeToFile(TString::Format("%s/%s",options.get<std::string>("workdir").data(),options.get<std::string>("outfilename").data()).Data());

  return 0;
}
