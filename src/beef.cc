//from std
#include <string>
#include <iostream>
#include <vector>
//from BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/replace.hpp>
//from ROOT
#include "TROOT.h"
#include "TStyle.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TSystem.h"
#include "TStopwatch.h"
#include "TError.h"
//from ROOFIT
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooDataSet.h"
#include "RooConstVar.h"
#include "RooFormulaVar.h"
#include "RooFitResult.h"
#include "RooCmdArg.h"
#include "RooLinkedList.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooMsgService.h"
//from ROOSTATS
#include "RooStats/SPlot.h"
//local
#include "../include/plotter.h"
#include "../IOjuggler/IOjuggler.h"

#ifdef BEEF_GIT_HASH
  #define BEEF_HASH BEEF_GIT_HASH
#else
  #define BEEF_HASH " "
#endif

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner(){return 0;}

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();

  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  auto options = parse_options(argc, argv, "c:d:hi:o:r:v:w:p:");
  MessageService msgsvc("beef",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.infomsg("Current beef git hash: " + static_cast<std::string>(BEEF_HASH));
  //open file, check for absolute path and a path with prepended workdir
  auto infile = get_file(options.get<std::string>("infilename"),options.get<std::string>("workdir"));
  auto w = get_obj<RooWorkspace>(infile,options.get<std::string>("wsname"));
  //get ptree
  pt::ptree configtree = get_ptree(options.get<std::string>("config"));

  //  replace {prefix} in config tree
  replace_stuff_in_ptree(configtree,"{prefix}",options.get<std::string>("prefix",""),"");
  //append and replace stuff in ptree
  auto_append_in_ptree(configtree);
  auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    print_ptree(configtree);

  if(configtree.get_optional<bool>("suppress_RootInfo")){
    gErrorIgnoreLevel = kWarning;
    RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
  }

  // get dataset
  auto dsFull = get_wsobj<RooDataSet>(w,options.get<std::string>("dsname"));
  if(configtree.get_optional<std::string>("weight")){
    if(!configtree.get_optional<bool>("fitoptions.SumW2Error")){
      msgsvc.warningmsg("You are working with a weighted dataset, but did not make use of the SumW2Error option.");
      msgsvc.warningmsg("The uncertainties returned by the fit will only be correct if your weights are properly normalised!");
    }
    if(configtree.get_optional<std::string>("weight_function")){
      auto weight_var = get_wsobj<RooRealVar>(w,configtree.get<std::string>("weight"));
      RooFormulaVar wFunc("wfunc","event weight",configtree.get<std::string>("weight_function").data(),*weight_var);
      auto addedwf = static_cast<RooRealVar*>(dsFull->addColumn(wFunc));
      dsFull = new RooDataSet(dsFull->GetName(),dsFull->GetTitle(),dsFull,*dsFull->get(),"",addedwf->GetName());
    }
    else
      dsFull = new RooDataSet(dsFull->GetName(),dsFull->GetTitle(),dsFull,*dsFull->get(),"",configtree.get<std::string>("weight").data());
  }
  msgsvc.infomsg(TString::Format("%.0f candidates in dataset", dsFull->sumEntries()));

  // apply cuts
  RooDataSet* ds = nullptr;
  if(auto selection = configtree.get_optional<std::string>("selection")){
    msgsvc.infomsg("Selection: " + *selection);
    ds = dynamic_cast<RooDataSet*>( dsFull->reduce(RooFit::Cut((*selection).c_str()),RooFit::Name(configtree.get("dsName","Dred").c_str())) );
  }
  else
    ds = new RooDataSet(*dsFull,configtree.get("dsName","Dred").c_str());
  msgsvc.infomsg(TString::Format("%.0f candidates selected", ds->sumEntries()));

  // Build model from factory strings in fitconfig file
  if(!configtree.get_child_optional("model")) throw std::runtime_error("child \"model\" not found in config file");
  msgsvc.infomsg("Model: ");
  for(const auto &v : configtree.get_child("model")) {
    std::string component = v.second.data();
    msgsvc.infomsg("      " + component);
    w->factory(component.c_str());
  }
  if(!configtree.get_optional<std::string>("pdf")) throw std::runtime_error("\"pdf\" not found in config file");
  auto modelPdf = get_wsobj<RooAbsPdf>(w,configtree.get<std::string>("pdf"));

  //get fitoptions
  RooLinkedList llist;
  std::vector<RooCmdArg*> args;
  args.push_back( new RooCmdArg( RooFit::Save() ) );//we want to save the fitresult, so this is not optional
  if(configtree.get_optional<bool>("fitoptions.Extended"))                   args.push_back( new RooCmdArg( RooFit::Extended()) );
  if(configtree.get_optional<bool>("fitoptions.SumW2Error"))                 args.push_back( new RooCmdArg( RooFit::SumW2Error(true)) );
  if(configtree.get_optional<bool>("fitoptions.InitialHesse"))               args.push_back( new RooCmdArg( RooFit::InitialHesse()) );
  if(configtree.get_optional<bool>("fitoptions.Minos"))                      args.push_back( new RooCmdArg( RooFit::Minos()) );
  if(configtree.get_optional<bool>("fitoptions.Verbose"))                    args.push_back( new RooCmdArg( RooFit::Verbose()) );
  if(configtree.get_optional<bool>("fitoptions.WarningsOff"))                args.push_back( new RooCmdArg( RooFit::Warnings(false)) );
  if(configtree.get_optional<bool>("fitoptions.Timer"))                      args.push_back( new RooCmdArg( RooFit::Timer()) );
  if(auto opt = configtree.get_optional<int> ("fitoptions.Strategy"))        args.push_back( new RooCmdArg( RooFit::Strategy(*opt) ) );
  if(auto opt = configtree.get_optional<int> ("fitoptions.PrintLevel"))      args.push_back( new RooCmdArg( RooFit::PrintLevel(*opt) ) );
  if(auto opt = configtree.get_optional<int> ("fitoptions.PrintEvalErrors")) args.push_back( new RooCmdArg( RooFit::PrintEvalErrors(*opt) ) );
  if(auto opt = configtree.get_optional<std::string>("fitoptions.MinimizerType"))
    args.push_back( new RooCmdArg( RooFit::Minimizer((*opt).c_str(),configtree.get("fitoptions.MinimizerAlg","migrad").c_str()) ) );
  if(auto opt = configtree.get_optional<int>("fitoptions.NumCPU"))
    args.push_back( new RooCmdArg( RooFit::NumCPU(*opt,configtree.get("fitoptions.StratCPU",0)) ) );

  // constrain fit parameters (optional)
  RooArgSet constrainFuncs;
  if(configtree.get_child_optional("constrainParams")) {
    for(auto v : configtree.get_child("constrainParams")) {

      std::string parName = v.first;
      double value = v.second.get<double>("value");

      // get parameter we want to constrain as RooRealVar
      auto iterArgs = modelPdf->getVariables()->iterator();
      RooRealVar* pdfPar = nullptr;
      while( (pdfPar = dynamic_cast<RooRealVar*>(iterArgs.Next())) )
        if(pdfPar->GetName() == parName) break;
      pdfPar->setVal(value);//set it as starting parameter

      if(auto error = v.second.get_optional<double>("error")){
        msgsvc.infomsg(TString::Format("Constraining %s to %.8g +- %.8g",parName.data(),value,*error));
        w->factory(("Gaussian::constr_"+parName+"("+parName+
                    ",mean_"+parName+"["+std::to_string(value)+"]"+
                    ",width_"+parName+"["+std::to_string(*error)+"])").data());
        constrainFuncs.add(*get_wsobj<RooAbsArg>(w,"constr_"+parName));
      }
      else{
        msgsvc.infomsg(TString::Format("Setting %s to %.8g",parName.data(),value));
        pdfPar->setConstant();
      }
    }
    args.push_back( new RooCmdArg(RooFit::ExternalConstraints(constrainFuncs)));
  }
  
  for (auto arg : args){
    //the next line forbids usage of RooCmdArgs as r-value,
    //also taking the adress of a reference and casting to TObject* can't be handled by RooFit, thus the pointers
    arg->setProcessRecArgs(true,false);
    llist.Add(arg);
  }
  
  RooFitResult* fitresult = nullptr;
  if(configtree.get_optional<bool>("fitoptions.FitAsHist")){
    msgsvc.infomsg("Converting dataset to histogram and performing a binned fit");
    //auto hist = ds->binnedClone(); <-- doesn't work...
    RooArgSet observables_for_hist;
    if(!configtree.get_child_optional("observables")) throw std::runtime_error("\"observables\" not found in config file");
    for(const auto& obs : configtree.get_child("observables")){
      auto myobs = get_wsobj<RooRealVar>(w,obs.first);
      myobs->setBins(configtree.get<int>(TString::Format("plots.%splot.bins",obs.first.c_str()).Data(),100));
      observables_for_hist.add(*myobs);
    }
    RooDataHist hist("hist","hist",observables_for_hist,*ds);
    fitresult = modelPdf->fitTo(hist,llist);
  }
  else
    fitresult = modelPdf->fitTo(*ds,llist);
  //delete pointers from heap
  for (auto arg : args)
    delete arg;
  args.clear();

  //save fit results to ptree in .info
  if (configtree.get("saveFitResultsAsInfo",0)) {
    pt::ptree ptree_fitres;
    auto it_var_fitres = modelPdf->getVariables()->createIterator();
    RooRealVar* var_fitres = dynamic_cast<RooRealVar*>(it_var_fitres->Next());
    while (var_fitres){
      std::string varname = var_fitres->GetName();
      double varval = var_fitres->getVal();
      double varerr = var_fitres->getError();
      ptree_fitres.add(varname + ".Value",varval);
      ptree_fitres.add(varname + ".Error",varerr);
      var_fitres = dynamic_cast<RooRealVar*>(it_var_fitres->Next());
    }
    auto outInfoFile = options.get<std::string>("workdir") + "/" + options.get<std::string>("outfilename");
    outInfoFile.replace(outInfoFile.end()-5,outInfoFile.end(),".info");
    pt::write_info(outInfoFile, ptree_fitres);
  }
  
  if(configtree.get_child_optional("extended")){
    //Now we can get the sWeights. For this we need to set all shape parameters constant.
    //In the fitconfig, every extended parameter starts with N. This will be used below
    //yes this looks horrible, but didn't find elegant solution of how to set an arbitrary set of RooRealVars constant
    std::vector<TString> shape_varnames;
    std::vector<TString> extended_varnames;
    auto it_var = modelPdf->getVariables()->createIterator();
    auto var = it_var->Next();
    while (var){
      TString varinfo = var->GetName();
      bool obs_or_ext = false;
      if(!configtree.get_child_optional("observables")) throw std::runtime_error("\"observables\" not found in config file");
      for(const auto& obs : configtree.get_child("observables")){
        if (static_cast<TString>(var->GetName()) == obs.first.c_str()){
          varinfo += " skipped (is observable)";
          obs_or_ext = true;
        }
      }
      for(const auto& extpar : configtree.get_child("extended")){
        if (static_cast<TString>(var->GetName()) == extpar.first.c_str()){
          extended_varnames.emplace_back(var->GetName());
          varinfo += " used for sWeights";
          obs_or_ext = true;
        }
      }
      if(!obs_or_ext){
        shape_varnames.emplace_back( var->GetName() );
        varinfo += " fixed";
      }
      msgsvc.infomsg(varinfo);
      var = it_var->Next();
    }

    for(auto varname : shape_varnames){
      auto shape_var = w->var(varname);
      shape_var->setConstant();
    }

    //put the yield-parameters into a RooArgSet which is input to the sPlot
    RooArgList extended_parameters;
    for(auto varname : extended_varnames)
      extended_parameters.add(*w->var(varname));

    RooStats::SPlot("dummy","An SPlot",*ds, modelPdf, extended_parameters );
  }

  infile->Close();
  auto outfile = TFile::Open( TString::Format("%s/%s",options.get<std::string>("workdir").data(),options.get<std::string>("outfilename").data()).Data() ,"RECREATE");
  outfile->cd();
  //create a new workspace to avoid seg-faults arising from cached pdfs like RooFFTConvPdf when trying to access the old workspace in the new file
  RooWorkspace ws("w",true);
  //sweights are saved by importing ds
  ws.import(*ds);
  ws.import(*fitresult);
  ws.import(*modelPdf);
  ws.Write();

  // create control plot
  if(configtree.get_child_optional("plots"))
    for(const auto& plot : configtree.get_child("plots"))
          plotter(ws,ds,modelPdf,outfile,
                  options.get<std::string>("workdir"),
                  options.get<std::string>("prefix",""),  // optional prefix default ""
                  plot.second,std::move(msgsvc));

  outfile->Delete("*");
  outfile->Close("R");

  clock.Stop();
  if(!configtree.get_optional<bool>("suppress_RootInfo"))
    clock.Print();

  return 0;
}
