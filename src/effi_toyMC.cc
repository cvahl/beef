//from std
#include <string>
#include <vector>
#include <cstdio>
//from BOOST
#include <boost/algorithm/string/replace.hpp>
#include <boost/property_tree/ptree.hpp>
//from ROOT
#include "TError.h"
#include "TFile.h"
#include "TH1D.h"
#include "TROOT.h"
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TSystem.h"
//from ROOFIT
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooDataSet.h"
#include "RooMsgService.h"
#include "RooRandom.h"
//local
#include "../IOjuggler/IOjuggler.h"

#ifdef BEEF_GIT_HASH
#define BEEF_HASH BEEF_GIT_HASH
#else
#define BEEF_HASH " "
#endif

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner(){return 0;}

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  //line buffer for nice logfiles
  char* buf = nullptr;
  std::setvbuf(stdout,buf,_IOLBF,8192);

  //get some stuff, IOjuggler takes care of this
  //parse options pass to executable
  auto options = parse_options(argc, argv, "c:d:ho:v:","nonoptions: signal and background efficiencies (in [0,1]),"
                                                           " number of toys (default 1k), seed (default 42)",2);
  MessageService msgsvc("effi_toyMC",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.infomsg("Current beef git hash: " + static_cast<std::string>(BEEF_HASH));
  auto wd = options.get<std::string>("workdir");
  auto ofn = options.get<std::string>("outfilename");
  //get ptree
  const auto cfgname = options.get<std::string>("config");
  pt::ptree configtree = get_ptree(cfgname);
  //get desired efficiencies from nonoptions
  const double input_sig_eff = std::stod(options.get_child("extraargs").front().second.data());
  auto eaiter = options.get_child("extraargs").begin();
  const double input_bkg_eff = std::stod((*boost::next(eaiter)).second.data());
  const int ntimes = options.get_child("extraargs").size() == 2 ? 1000 : std::stoi((*boost::next(eaiter,2)).second.data());
  const auto seed = options.get_child("extraargs").size() == 4 ? std::stoul(options.get_child("extraargs").back().second.data()) : 42;

  //append and replace stuff in ptree
  auto_append_in_ptree(configtree);
  auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    print_ptree(configtree);

  if(configtree.get_optional<bool>("suppress_RootInfo")){
    gErrorIgnoreLevel = kWarning;
    RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
  }

  //check if directory exists. create it if not
  const auto dumpdir = wd+"/effi_toyMC";  
  if(!gSystem->OpenDirectory(dumpdir.data()))
      gSystem->mkdir(dumpdir.data());
  //open file, check for absolute path and a path with prepended workdir
  TFile f((dumpdir+"/"+ofn+".root").data(),"RECREATE");
  RooWorkspace dumpws("dumpws",true);

  //get observables
  dumpws.defineSet("observables","");
  if(!configtree.get_child_optional("observables"))
    throw std::runtime_error("\"observables\" not found in config file");
  for(const auto& obs : configtree.get_child("observables")){
    if(!obs.second.get_optional<double>("low")||!obs.second.get_optional<double>("high"))
      throw std::runtime_error("range for observable has not been found in the config file");
    RooRealVar myobs(obs.first.data(),obs.first.data(),obs.second.get<double>("low"),obs.second.get<double>("high"));
    dumpws.import(myobs);
    dumpws.extendSet("observables",obs.first.data());
  }

  // Build model from factory strings in fitconfig file
  auto build_model = [&dumpws,&msgsvc] (const auto& model_name) {
    if(!model_name)
      throw std::runtime_error("a required model has not been found in the config file");
    msgsvc.infomsg("Model: ");
    for(const auto &v : *model_name) {
      const auto component = v.second.data();
      msgsvc.infomsg("      " + component);
      dumpws.factory(component.data());
    }
  };

  build_model(configtree.get_child_optional("model_total"));
  //some sanity checks of the config file
  const auto model_name  = configtree.get_optional<std::string>("pdf");
  const auto nsig_name   = configtree.get_optional<std::string>("NSigName");
  const auto nbkg_name   = configtree.get_optional<std::string>("NBkgName");
  const auto sigpdf_name = configtree.get_optional<std::string>("SigPdfName");
  const auto bkgpdf_name = configtree.get_optional<std::string>("BkgPdfName");
  if(!model_name) throw std::runtime_error("\"pdf\" not found in config file");
  if(!nsig_name || !nbkg_name || !sigpdf_name || !bkgpdf_name)
    throw std::runtime_error("\"NSigName\", \"NbkgName\", \"SigPdfName\" or \"BkgPdfName\" not found in config file");

  //lazy way to get number of events to simulate
  const auto input_Nsig = get_wsobj<RooRealVar>(&dumpws,*nsig_name+"_total")->getVal();  
  const auto input_Nbkg = get_wsobj<RooRealVar>(&dumpws,*nbkg_name+"_total")->getVal();
  msgsvc.debugmsg("generated Nsig "+std::to_string(input_Nsig)+" and Nbkg "+std::to_string(input_Nbkg));
  auto sigPDF = get_wsobj<RooAbsPdf>(&dumpws,*sigpdf_name+"_total");
  auto bkgPDF = get_wsobj<RooAbsPdf>(&dumpws,*bkgpdf_name+"_total");

  //before we actually start, take care that the random numbers are really random
  TRandom3 rand(seed);
  //avoid that RooAbsPdf::generate() calls the most epic line of code that has ever been written:
  //line 34: https://root.cern.ch/doc/master/TRandom3_8h_source.html
  seed == 0 ? RooRandom::randomGenerator()->SetSeed(0) : RooRandom::randomGenerator()->SetSeed(1337+seed);
  auto counter = 0u;

  //important histos
  TH1D pulls       ("pulls",";#frac{#varepsilon_{sig,effi}-#varepsilon_{sig,gen}}{#Delta#varepsilon_{sig,effi}};Entries/0.1",100,-5,5);
  TH1D eff_reco    ("eff_reco",";#varepsilon_{sig,effi};Entries",100,-1,-1);
  TH1D delta_eff_hi("delta_eff_hi",";#Delta^{+}#varepsilon_{sig,effi};Entries",100,-1,-1);
  TH1D delta_eff_lo("delta_eff_lo",";#Delta^{-}#varepsilon_{sig,effi};Entries",100,-1,-1);
  TH1D delta_eff   ("delta_eff",";#bar{#Delta}#varepsilon_{sig,effi};Entries",100,-1,-1);
  TH1D eff_gen     ("eff_gen",";#varepsilon_{sig,gen};Entries",100,-1,-1);
  TH1D deff        ("deff",";#varepsilon_{sig,effi}-#varepsilon_{sig,gen};Entries",100,-1,-1);

  //the big loop
  for(int i = 0; i < ntimes; i++){

    msgsvc.infomsg("At toy number " + std::to_string(i));

    //make new workspace, so stuff doesn't get overwritten
    const auto infname = wd+"/effi_toyMC/ws_"+ofn+"_"+std::to_string(i)+".root";
    TFile innerf(infname.data(),"RECREATE");
    RooWorkspace w = *static_cast<RooWorkspace*>(dumpws.Clone("w"));
    if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) w.Print();

    //generate some signal and background samples
    msgsvc.debugmsg("RooFit seed signal " + std::to_string(RooRandom::randomGenerator()->GetSeed()));
    auto sig_data = sigPDF->generate(*dumpws.set("observables"),input_Nsig,RooFit::Extended(true));
    RooRealVar pass("pass","pass",0,1);
    RooDataSet pass_sig_ds("pass_sig_ds","pass_sig_ds",pass);
    counter = 0u;
    const auto Nsig_gen = sig_data->numEntries();
    for(int j = 0; j < Nsig_gen;j++){
      if(rand.Uniform() > input_sig_eff){ pass.setVal(0); counter++; }
      else pass.setVal(1);
      pass_sig_ds.add(pass);
    }
    const auto sig_eff_gen = static_cast<double>(Nsig_gen-counter)/static_cast<double>(Nsig_gen);
    msgsvc.debugmsg("Generated signal efficiency: " + std::to_string(sig_eff_gen));

    msgsvc.debugmsg("RooFit seed background " + std::to_string(RooRandom::randomGenerator()->GetSeed()));
    auto bkg_data = bkgPDF->generate(*dumpws.set("observables"),input_Nbkg,RooFit::Extended(true));
    RooDataSet pass_bkg_ds("pass_bkg_ds","pass_bkg_ds",pass);
    counter = 0u;
    const auto Nbkg_gen = bkg_data->numEntries();
    for(int j = 0; j < Nbkg_gen;j++){
      if(rand.Uniform() > input_bkg_eff){ pass.setVal(0); counter++; }
      else pass.setVal(1);
      pass_bkg_ds.add(pass);
    }
    const auto bkg_eff_gen = static_cast<double>(Nbkg_gen-counter)/static_cast<double>(Nbkg_gen);
    msgsvc.debugmsg("Generated background efficiency: " + std::to_string(bkg_eff_gen));

    //merge pass, signal and bkg data, put it into a workspace
    sig_data->merge(&pass_sig_ds);
    bkg_data->merge(&pass_bkg_ds);
    RooDataSet data_comb(*sig_data);
    data_comb.append(*bkg_data);
    data_comb.SetName("ds");
    if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) data_comb.Print();    
    w.import(data_comb);

    w.Write();
    innerf.Close();
    const auto outfname = "effi_toyMC/res_"+ofn+"_"+std::to_string(i)+".root";

    //call effi
    auto fit_status = std::system(("build/bin/effi"
                                   " -c " + cfgname +
                                   " -i " + infname +
                                   " -o " + outfname +
                                   " -d " + wd +
                                   " -v " + std::to_string(configtree.get("effi_verbosity",options.get<int>("verbosity"))) +
                                   " 'pass > 0.5'").data());
    if(fit_status != 0){
      msgsvc.warningmsg("Some fitting problems. Will not add this result to pulls.");
    }
    else{
      msgsvc.debugmsg("Good fit. Adding results to pulls");
      pt::ptree resulttree = get_ptree(wd+"/"+boost::algorithm::replace_all_copy(outfname,".root",".info"));
      const auto eff_fit = resulttree.get<double>("sig_eff.Value",-1.0*ntimes);
      const auto delta_eff_fit_hi = resulttree.get<double>("sig_eff.ErrorHi",-9999.9);
      const auto delta_eff_fit_lo = -1*resulttree.get<double>("sig_eff.ErrorLo",9999.9);
      const auto num = eff_fit-sig_eff_gen;
      num >= 0 ? pulls.Fill(num/delta_eff_fit_lo) : pulls.Fill(num/delta_eff_fit_hi);
      eff_reco    .Fill(eff_fit);
      delta_eff_hi.Fill(delta_eff_fit_hi);
      delta_eff_lo.Fill(delta_eff_fit_lo);
      delta_eff   .Fill(0.5*(delta_eff_fit_hi+delta_eff_fit_lo));
      eff_gen     .Fill(sig_eff_gen);
      deff        .Fill(num);
    }
  }
  f.cd();
  msgsvc.debugmsg("Writing results");
  pulls       .Write();
  eff_reco    .Write();
  delta_eff_hi.Write();
  delta_eff_lo.Write();
  delta_eff   .Write();
  eff_gen     .Write();
  deff        .Write();

  f.Close();
  return 0;
}
