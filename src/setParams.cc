/* @brief puts parameter values from external fitresults file into fitconfig for constrained fits
 *
 */

//from std
#include <string>
#include <iostream>
//from BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/optional/optional.hpp>
//from ROOT
#include "TROOT.h"
#include "TString.h"
#include "TFile.h"
//from ROOFIT
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooDataSet.h"
#include "RooCmdArg.h"
//local
#include "../IOjuggler/IOjuggler.h"

#ifdef BEEF_GIT_HASH
  #define BEEF_HASH BEEF_GIT_HASH
#else
  #define BEEF_HASH " "
#endif

namespace pt = boost::property_tree;

int main(int argc, char** argv){

  auto options = parse_options(argc, argv, "i:o:w:hv:", "Mandatory additional parameter: <child1:fitresultsfile1>\n optional: <child2:fitresultsfile2> ...", 1);

  MessageService msgsvc("setParams",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.infomsg("Current beef git hash: " + static_cast<std::string>(BEEF_HASH));

  auto configtree = get_ptree(options.get<std::string>("infilename"));
  
  boost::optional <pt::ptree&> setParsNode = configtree.get_child_optional("setParams");
  if(setParsNode==0) {
    msgsvc.errormsg("Missing setParams node in fitconfig");
    return 1;
  }

  for (auto item : options.get_child("extraargs")) {

    TString extraoptStr = item.second.data();
    TObjArray *extraoptArr = extraoptStr.Tokenize(":");

    if(extraoptArr->GetEntries() != 2) {
      msgsvc.errormsg("Invalid syntax of command line arguments.\nSyntax is: setParams -i <inputfile> -o <outputfile> <childName1:fitresultsFile1> [<childName2:fitresultsFile2> ...]");
      return 1;
    }
    
    TString paramsNodeNameTStr = ((TObjString *)(extraoptArr->At(0)))->GetString();
    std::string paramsNodeName = paramsNodeNameTStr.Data();
    std::string paramsNodeNameComplete = "setParams." + paramsNodeName;
    boost::optional <pt::ptree&> parChild = configtree.get_child_optional(paramsNodeNameComplete);
    if(!parChild) {
      msgsvc.errormsg("Missing child \'" + paramsNodeName + "\' in setParams child in fitconfig");
      return 1;
    }

    TString fitresfileTStr = ((TObjString *)(extraoptArr->At(1)))->GetString();
    //string fitresfileStr = static_cast<string>(fitresfileTStr.Data());
    //auto fToGet = get_file(fitresfileStr);
    TFile *fToGet = TFile::Open(fitresfileTStr);
    auto wsToGet = get_obj<RooWorkspace>(fToGet,options.get<std::string>("wsname"));
    if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG) wsToGet->Print();

    // loop over all parameters to set
    for(auto it : setParsNode->get_child(paramsNodeName)){
      if(!it.second.data().empty()){//check if this is a valid key value pair
        msgsvc.debugmsg("At " + it.first);
        TString temp = it.first;
        std::string varToSet_name;
        bool dont_fix = true;
        if(temp.BeginsWith("fix")){
          dont_fix = false;
          varToSet_name = static_cast<std::string>(temp.Remove(0,3));
        }
        else varToSet_name = it.first;
        auto varToGet = get_wsobj<RooRealVar>(wsToGet,it.second.data());

        configtree.put("constrainParams." + varToSet_name + ".value", std::to_string(varToGet->getVal()));
        if(dont_fix)
          configtree.put("constrainParams." + varToSet_name + ".error", std::to_string(varToGet->getError()) );
      }
      else{
        //now it really gets ugly: a problem in LcD0K arose, when fixing the charmless yield,
        //because the yield was determined in a 3D fit using a 6 sigma window around the fitted D0 mass
        //and we want to fix this yield in a fit where there is a 2.58 sigma mass cut on the D0.
        //As the charmless yield is flat in the Kpi mass, one could simply multiply the yield by 2.58/6.
        //The problem then is, that you can't simply multiply factors to fixed parameters with the workspace factory in RooFit.
        //So as we have to do this here now, let's do it right and calculate a real integral
        if(auto integral_child = configtree.get_child_optional(paramsNodeNameComplete + ".yield_from_integral")){
          for(const auto& integralterator : *integral_child){
            if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG) print_ptree(integralterator.second);
            //get names for ratio of integrals
            const auto intvn  = integralterator.second.get_optional<std::string>("integrate");
            const auto pdfn   = integralterator.second.get_optional<std::string>("pdf");
            const auto lo_lim = integralterator.second.get_optional<double>("from");
            const auto hi_lim = integralterator.second.get_optional<double>("to");
            if(!intvn || !pdfn || !lo_lim || !hi_lim){
              msgsvc.errormsg("Missing one of the mandatory \"integrate\" \"pdf\" \"from\" \"to\" keys in \"yield_from_integral\" node");
              return 1;
            }
            //get variable and pdf for ratio of integrals
            auto tmpobs = get_wsobj<RooRealVar>(wsToGet,*intvn);
            const auto tmppdf = get_wsobj<RooAbsPdf>(wsToGet,*pdfn);
            //now get variable to constrain or fix
            const auto fvn = integralterator.second.get_optional<std::string>("fix");
            const auto cvn = integralterator.second.get_optional<std::string>("constrain");
            if(!(fvn || cvn)){
              msgsvc.errormsg("Missing mandatory \"fix\" or \"constrain\" in \"yield_from_integral\" node");
              return 1;
            }
            const auto focvn = fvn ? *fvn : *cvn;
            const auto varfoc = get_wsobj<RooRealVar>(wsToGet,focvn);
            //get ratio of integrals
            const auto ntocof = integralterator.second.get("newname",focvn);
            tmpobs->setRange(("SignalRange"+ntocof).data(),*lo_lim,*hi_lim);
            const auto roi = tmppdf->createIntegral(*tmpobs,RooFit::NormSet(*tmpobs),RooFit::Range(("SignalRange"+ntocof).data()));
            //put result in output config
            const auto roival = roi->getVal();
            const auto bvalfoc = std::to_string(varfoc->getVal()*roival);
            configtree.put("constrainParams." + ntocof + ".value", bvalfoc);
            msgsvc.infomsg("Integral of " + *intvn + " in [" + std::to_string(*lo_lim) + "," + std::to_string(*hi_lim) + "] w.r.t full range: " + std::to_string(roival));
            if(cvn){
              //there is no "native" uncertainty from the integral
              //const auto uncstr = std::to_string(std::sqrt(std::pow(roival*varfoc->getError(),2)+std::pow(roi->getError()*varfoc->getVal(),2)));
              //so it's neglected...
              const auto uncstr = std::to_string(roival*varfoc->getError());
              msgsvc.infomsg("Constraining " + focvn + " as " + ntocof + " to [" + bvalfoc + "-" + uncstr + "," + bvalfoc + "+" + uncstr + "]");
              configtree.put("constrainParams." + ntocof + ".error", uncstr);
            }
            else
              msgsvc.infomsg("Fixing " + focvn + " as " + ntocof + " to " + bvalfoc);

          }
        }
      }
    }
    
  } // end loop over params to set
  
  pt::write_info(options.get<std::string>("outfilename").data(), configtree);
  
  return 0;
}
