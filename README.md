# Contributing and Reusing
This is an open analysis tool. Please have a look at our [guidelines](CONTRIBUTING.md) for contributions. If you want to reuse this code in your own project please note the [reciprocity](RECIPROCITY.md) statement.

# Quick start

Build and install `beef` (on LxpLus)
```
> SetupProject ROOT
> export CC=gcc
> export CXX=gcc
> mkdir build
> cd build
> cmake -DCMAKE_INSTALL_PREFIX=$HOME ../ 
> make && make install
> export PATH=$HOME/bin:$PATH
> cd ..
```

Generate some toy data:
```
> root -b -q src/generateToy.C
```

Create workspace
```
> createWorkspace -i /toy.root -c config/config.info -o /toyws.root -t toy
```
Have a look at the config file to see how to define more complex workspaces.

Perform the fit defined in `config/fitconfig.info`
```
> beef -c config/fitconfig.info -i toyws.root -o toyfit.root
```

Look at the fitresults (png, C and pdf files are created automatically).
The dataset including sweights, model etc. are saved in the workspace of the output file.
Inspect it with
```
> root -l toyfit.root
root [1] _file0->ls()
root [2] RooWorkspace* w = static_cast<RooWorkspace*>(_file0->Get("w"))
root [3] w->Print()
```
# Writing config files
To configure `beef`, two options files are needed. <br>
One to set up the [RooWorkspace](https://root.cern.ch/doc/master/classRooWorkspace.html "RooWorkspace") which contains all relevant data, defines the observable, and can be used to setup custom pdfs. <br>
The other config file is parsed when running the fit and producing a plot. <br>
The usage of the options files is documented in [config/README_workspace_config.md](config/README_workspace_config.md) and [config/README_fit_config.md](config/README_fit_config.md).
