# Documentation for effi
Extracting data-driven efficiencies is a challenging task. There are several sources of potential systematic uncertainties.
The presence of combinatorial background, non-factorisation of signal and background pdfs as a function of control variables,
statistical fluctuations due to limited statistics and unknown shapes of signal and background pdfs.<br>
The goal of effi is to provide a tool to extract unbiased efficiencies with a desired coverage from data in the presence of physical backgrounds.
To ensure unbiased efficiency estimation and good coverage properties, an easy to use toyMC tool can be run to test
effi's settings for a given model at an expected signal and background efficiency. The toyMC tool uses the same input as effi.<br>
Sequiential fits to total and pass datasets are used, where the efficiency is a floating fit parameter in the fit to the passed dataset.
When fitting the passed dataset, constraints to yield and shape parameters (i.e. parameters of interest and nuisance parameters) can be applied.<br>
***ATTENTION***: although it was tried extensively to get effi to produce meaningful confidence intervals, nothing suceeded.<br>
Currently the strategy is to compute uncertainties from toyMC.<br>
The following was tried:
 - Fitting total and pass yields, and using TEfficiency to calculate an effective number of events that takes nuisance paramters into account
 (worked in a signal only case with artificial nuisance parameters.
 See [the simulation code](https://gitlab.cern.ch/mstahl/beef/blob/dirty_effi/src/TEff_toyMC.cpp) and [the correction factor calculation in the plotting script](https://gitlab.cern.ch/mstahl/beef/blob/dirty_effi/src/TEff_scan_plots.cpp#L46))
 - Simultaneous fits (Not as flexible. There are examples where the background shape or the signal width changes with the applied cut)
 - More involved statistics methods:
     - Roofits profile likelihood scans resulted in the same results as the much faster Minos algorithm. Both have a bad coverage
     - The `ProfileLikelihoodCalculator` relies on asymptotic formulae which are known to fail when applying constraints
     - The `BayesianCalculator` simply takes too long. Markov chain methods and Feldman Cousins were not tried for the same reason

## Arguments for effi
- *-c* : config file
- *-d* : work directory
- *-i* : input file (with a RooWorkspace and RooDataSet(s))
- *-o* : ouput file name
- *-r* : name of RooDataSet (Default "ds". If fold number is given, it will be added as suffix to the name)
- *-w* : name of RooWorkspace (Default "w")
- *-v* : verbosity level (3 for DEBUG, 2 for INFO, 1 for WARNING, 0 for ERROR)
- nonoptions : cut that defines the pass criterion (mandatory)

## Arguments for effi_toyMC
**Attention** If you want to run toys, you need to run cmake with the `-DEFFITOYMC=ON` flag.
- *-c* : config file
- *-d* : work directory
- *-o* : ouput file name (without file name extension!)
- *-v* : verbosity level (3 for DEBUG, 2 for INFO, 1 for WARNING, 0 for ERROR)
- nonoptions : signal and background efficiencies (in [0,1]) (mandatory), number of toys (default 1k), seed for random number generator (default 42)

In the toyMC config file, there is one unique self explaining command that is not worth putting in a single paragraph: `effi_verbosity`

## Writing config files for effi:
Basically all options used in [beef](README_fit_config.md) are known to effi as well.<br>

In the modelling part, the pass and total model have to be given, as well as the signal and background composition. It should look something like
```
model_total {
 Signal     "SUM::gLb_total(Gaussian(mLb,mmLb_total[5621,5610,5635],sigma1Lb_total[12.3,2,22]),fg_total[0.6,0,1]*Gaussian(mLb,mmLb_total,sigma2Lb_total[7.14,0,22]))"
 Background "Chebychev::bgLb_total(mLb,{aLb_total[-0.22,-2.0,2.0]})"
 Model      "SUM::modelLb_total(NLb_total[5500,0,2e+4]*gLb_total,NbgLb_total[1500,0,2e+4]*bgLb_total)"
}
model_pass {
 Signal     "SUM::gLb_pass(Gaussian(mLb,mmLb_pass[5620,5600,5650],sigma1Lb_pass[14,2,22]),fg_pass[0.6,0,1]*Gaussian(mLb,mmLb_pass,sigma2Lb_pass[7,0,22]))"
 Background "Chebychev::bgLb_pass(mLb,{aLb_pass[-0.4,-2.0,2.0]})"
}
NSigName   "NLb"
NBkgName   "NbgLb"
SigPdfName "gLb"
BkgPdfName "bgLb"
```
There are a few important (rather arbitrary and improvable) things in these lines:
 - The total model should look like `N_sig*PDF_sig + N_bkg*PDF_bkg`
 - Total and pass components have the same name-stub: `gLb` or `bgLb` and `_total` or `_pass` in addition. It has to be given in this way
 - Yields are only given to the total model, the pass yields/efficiency is specified internally
 - The name of the total yield also has to have an additional `_total`
<br>
Special parameters of this tool to steer constraints and conditional observables are the following:

```
beta_constraint                  1                                  ; apply a constraint to the pass yield (using a beta pdf https://root.cern.ch/doc/master/group__PdfFunc.html#gad73849a30c807d67c0ea8d8c4b5d4ee9)
beta_par                         "1.0"                              ; set a and b parameters of this beta pdf
constrain_nuisance_parameters    1                                  ; constrain nuisance parameters by a Gaussian with mean at the corresponding fitted total value
exclude_nuisance_parameters                                         ; child containing names of parameters to be excluded from constraining (only has effect if constrain_nuisance_parameters is set; by default all parameters which are not NSigName or NBkgName are constrained)
nuisance_parameters_to_constrain                                    ; child containing names of parameters to be constrained (use either this to explicitly give the parameters you want to constrain, or the child above to steer which parameters will be constraint)
sig_constr                       "expr('PAR_ERR*(1.0-@0)',eff_sig)" ; set the width of the Gaussian constraint with this factory-like command. [factory doc](https://root.cern.ch/doc/master/classRooFactoryWSTool.html#a228d9cb336a3bdc7dfe260f4c6aaf8df)
bkg_constr                       "expr('PAR_ERR*(1.0-@0)',eff_bkg)" ; PAR_ERR will be replaced by the value of the error on the nuisance parameter from the total fit.
binomial_constraint              1                                  ; apply a binomial constraint in the pass fit (signal and background). Uses the fixed number of total events, the floating efficiency and N_tot*eff as parameters
pray                             1                                  ; try to fix non-converged fits in case of extreme efficiencies
effi_verbosity                   1                                  ; set effi vebosity in toyMC config files (it is the toyMC vebosity by default)
```
