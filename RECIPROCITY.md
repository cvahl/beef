# Re-Use of this analysis code
By using parts or all of the code in this repository ('the code' from here on), you agree with the following reciprocity statement.

Projects reusing the code must make their own analysis code available at least to the same group of users that the original copy of the code was available to. This extends to all functional parts of the project.