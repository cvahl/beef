//from std
#include <iostream>
#include <memory>
#include <string>
#include <utility>
//from BOOST
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
//from ROOT
#include "TAxis.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TLine.h"
#include "TString.h"
#include "TStyle.h"
//from ROOFIT
#include "RooAbsPdf.h"
#include "RooAbsData.h"
#include "RooCmdArg.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooHist.h"
#include "RooLinkedList.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooWorkspace.h"
//local
#include "lhcbStyle.C"
#include "../IOjuggler/IOjuggler.h"

namespace pt = boost::property_tree;

template <class M>
void plotter(const RooWorkspace& ws, const RooDataSet* ds, const RooAbsPdf* modelPdf, TFile* outfile,
             const std::string &&workdir, 
             const std::string &&prefix,
             const pt::ptree& plotnode, M&& msgsvc){

  outfile->cd();
  auto Observable = get_wsobj<RooRealVar>(&ws,plotnode.get<std::string>("var"));
  // set basic plot style
  lhcbStyle(static_cast<int>(msgsvc.GetMsgLvl()));

  const float textsize       = plotnode.get<float>("textsize",0.05);
  const float Left_margin    = plotnode.get<float>("lmargin",0.11);
  const float Top_margin     = plotnode.get<float>("tmargin",0.01);
  const float Right_margin   = plotnode.get<float>("rmargin",0.01);
  const float Bigy_margin    = plotnode.get<float>("bmargin",0.12);
  const float padsplit       = plotnode.get<float>("pullheight",0.32);
  const float padsplitmargin = 1e-6;//leave this hardcoded as it's the only thing making sense in this layout

  TCanvas c1("c1","Fit",plotnode.get("CanvXSize",2048),plotnode.get("CanvYSize",1280));

  //Must control the margins in the individual pads and not the subpads of the canvas, because of the axis labels
  gPad->SetTopMargin   (padsplitmargin);
  gPad->SetBottomMargin(padsplitmargin);
  gPad->SetRightMargin (padsplitmargin);
  gPad->SetLeftMargin  (padsplitmargin);
  if(auto mdigs = plotnode.get_optional<int>("MaxDigits"))TGaxis::SetMaxDigits(*mdigs);

  TPad pad1("pad1","pad1",padsplitmargin,padsplit,1-padsplitmargin,1-padsplitmargin);
  pad1.Draw();
  pad1.cd();
  pad1.SetBorderSize  (0);
  pad1.SetLeftMargin  (Left_margin);
  pad1.SetRightMargin (Right_margin);
  pad1.SetTopMargin   (Top_margin);
  pad1.SetBottomMargin(padsplitmargin);
  pad1.SetTickx();
  pad1.SetTicky();
  if(plotnode.get_optional<bool>("logy"))
    pad1.SetLogy();
  c1.Update();

  auto frame = Observable->frame(plotnode.get<double>("low"), plotnode.get<double>("high"), plotnode.get<int>("bins"));

  ds->plotOn(frame,
             RooFit::MarkerSize (plotnode.get("MSizeData",1.f)),
             RooFit::MarkerColor(LHCb::color(plotnode.get("MColorData","black"))),
             RooFit::LineWidth  (plotnode.get("LWidthData",1)),
             RooFit::LineColor  (LHCb::color(plotnode.get("LColorData","black"))),
             RooFit::DrawOption (plotnode.get("OptionData","e1p").data()),
             RooFit::DataError  (static_cast<RooAbsData::ErrorType>(plotnode.get<int>("DataError",3)))
             );

  //add pdf projections
  for(const auto& v: plotnode.get_child("graphs")) {
    RooLinkedList ledlist;
    std::vector<RooCmdArg*> args;
    if(v.second.get_optional<bool>("vlines"))    args.push_back( new RooCmdArg(RooFit::VLines()) );
    if(v.second.get_optional<bool>("invisible")) args.push_back( new RooCmdArg(RooFit::Invisible()) );
    args.push_back(new RooCmdArg(RooFit::Components( v.second.get<std::string>("components").data()))); //the component has to be given!
    args.push_back(new RooCmdArg(RooFit::Name      ((v.second.get<std::string>("components") + "P").data())));
    args.push_back(new RooCmdArg(RooFit::LineColor (LHCb::color(v.second.get("linecolor","blue"))))); //set color to blue if not specified
    args.push_back(new RooCmdArg(RooFit::LineStyle (v.second.get("style",1))));
    args.push_back(new RooCmdArg(RooFit::LineWidth (v.second.get("linewidth",3))));
    args.push_back(new RooCmdArg(RooFit::FillColor (LHCb::color(v.second.get("fillcolor","blue")))));
    args.push_back(new RooCmdArg(RooFit::DrawOption(v.second.get("option","L").data())));
    args.push_back(new RooCmdArg(RooFit::AddTo     (v.second.get("addto","").data(),1,1)));
    for (auto arg : args){
      arg->setProcessRecArgs(true,false);
      ledlist.Add(arg);
    }
    modelPdf->plotOn(frame,ledlist);

    //as RooFit::VLines() doesn't do what it's supposed to, let's try something else
    if(v.second.get_optional<bool>("manual_vlines")){
      auto gr = static_cast<TGraph*>(frame->findObject((v.second.get<std::string>("components") + "P").data()));
      double gx = 0, gy = 0;
      gr->GetPoint(gr->GetN()-1,gx,gy);
      //The problem only is, that the last point is not set to 0
      gr->SetPoint(gr->GetN()-1,gx,0);
    }

    for (auto arg : args)
      delete arg;
    args.clear();
  }

  //draw second time since pdf projections may overlay parts of the data
  ds->plotOn(frame,
             RooFit::Name       (TString::Format("%sP",ds->GetName()).Data()),
             RooFit::MarkerSize (plotnode.get("MSizeData",1.f)),
             RooFit::MarkerColor(LHCb::color(plotnode.get("MColorData","black"))),
             RooFit::LineWidth  (plotnode.get("LWidthData",1)),
             RooFit::LineColor  (LHCb::color(plotnode.get("LColorData","black"))),
             RooFit::DrawOption (plotnode.get("OptionData","e1p").data()),
             RooFit::DataError  (static_cast<RooAbsData::ErrorType>(plotnode.get<int>("DataError",3)))
             );

  //add optional legends
  //need to store them in a vector since the legends in the loop go out of scope and cause seg-fault when frame->Draw() is called
  std::vector< std::unique_ptr<TLegend> > legs;
  if(plotnode.get_child_optional("legends")){
    for(const auto& l : plotnode.get_child("legends")) {
      std::unique_ptr<TLegend> leg( new TLegend(l.second.get<double>("xlo"),
                                                1-gPad->GetTopMargin()-l.second.get<double>("dtop")-(l.second.get<double>("height")*textsize/(1-padsplit)),
                                                1-gPad->GetRightMargin()-l.second.get<double>("dright"),
                                                1-gPad->GetTopMargin()-l.second.get<double>("dtop"),
                                                static_cast<std::string>(l.second.get("header","")).data(),"brNDC"));
      leg->SetBorderSize(0);
      leg->SetFillColor(kWhite);
      leg->SetTextAlign(12);
      leg->SetFillStyle(0);
      leg->SetTextSize(l.second.get<double>("scale",1.f)*textsize/(1-padsplit));
      for(const auto& v : l.second ) {
        if( v.second.get_optional<std::string>("name") )
          leg->AddEntry(frame->findObject(v.second.get<std::string>("name").data()),
                        v.second.get<std::string>("label").data(),
                        v.second.get<std::string>("option","lpf").data());
      }
      legs.push_back(std::move(leg));
    }
  }
  for(const auto& leg : legs)
    frame->addObject(leg.get());

  //Hide potential exponent drawn at the right side of the axis
  TPaveText HideExpXP1(1.002-pad1.GetRightMargin(),pad1.GetBottomMargin(),1,pad1.GetBottomMargin()+0.1,"BRNDC");
  HideExpXP1.SetBorderSize(0);HideExpXP1.SetFillColor(kWhite);HideExpXP1.SetFillStyle(1001);HideExpXP1.SetTextColor(kWhite);
  HideExpXP1.AddText(" ");
  frame->addObject(&HideExpXP1);

  //Hide potential exponent drawn on top
  TPaveText HideExpYP1(pad1.GetLeftMargin(),1.002-pad1.GetTopMargin(),pad1.GetLeftMargin()+0.1,1,"BRNDC");
  HideExpYP1.SetBorderSize(0);HideExpYP1.SetFillColor(kWhite);HideExpYP1.SetFillStyle(1001);HideExpYP1.SetTextColor(kWhite);
  HideExpYP1.AddText(" ");
  if(plotnode.get_optional<bool>("HideExpY"))
    frame->addObject(&HideExpYP1);

  frame->GetYaxis()->SetTitleSize  (textsize/(1-padsplit));
  frame->GetYaxis()->SetLabelSize  (textsize/(1-padsplit));
  frame->GetYaxis()->SetTitleOffset(plotnode.get("yOffset",1.2)*(1-padsplit));
  const auto parsed_ymax = plotnode.get("ymax",frame->GetMaximum() + plotnode.get("ymaxadd",0.f)*(frame->GetMaximum() - plotnode.get("ymin",0.1)));
  frame->GetYaxis()->SetRangeUser  (plotnode.get("ymin",0.1),parsed_ymax);

  TString yaxistitle;
  const auto binsize = (plotnode.get<double>("high") - plotnode.get<double>("low"))/plotnode.get<double>("bins");
  if(auto ytitle = plotnode.get_optional<std::string>("ytitle"))yaxistitle = *ytitle;
  else if(parsed_ymax > std::pow(10,plotnode.get("MaxDigits",3)) && parsed_ymax < 1e+6){
    if(!plotnode.get_optional<int>("MaxDigits")) TGaxis::SetMaxDigits(3);//adjust digits
    yaxistitle.Form("10^{3} Events/%.3g MeV",binsize);
  }
  else if(parsed_ymax > 1e+6){
    if(!plotnode.get_optional<int>("MaxDigits")) TGaxis::SetMaxDigits(6);//adjust digits
    yaxistitle.Form("10^{6} Events/%.3g MeV",binsize);
  }
  else if(parsed_ymax < std::pow(10,plotnode.get("MaxDigits",3)))
    yaxistitle.Form("Events/%.3g MeV",binsize);
  else
    yaxistitle = "";
  frame->GetYaxis()->SetTitle(yaxistitle);

  frame->SetName ("fit");
  frame->SetTitle("");
  frame->Draw    ();
  frame->Write   ();

  //go back to canvas and start using the second pad
  c1.cd();

  TPad pad2("pad2","pad2",padsplitmargin,padsplitmargin,1-padsplitmargin,padsplit);
  pad2.SetBorderSize  (0);
  pad2.SetLeftMargin  (Left_margin);
  pad2.SetRightMargin (Right_margin);
  pad2.SetTopMargin   (padsplitmargin);
  pad2.SetBottomMargin(Bigy_margin/padsplit);
  pad2.SetTickx();
  pad2.SetTicky();

  // Construct a histogram with the pulls of the data w.r.t the curve
  RooHist* hpull = frame->pullHist() ;
  hpull->SetMarkerSize (plotnode.get("MSizePull",1.f));
  hpull->SetMarkerColor(LHCb::color(plotnode.get("MColorPull","azure+3")));
  hpull->SetLineWidth  (plotnode.get("LWidthPull",1.f));
  hpull->SetLineColor  (LHCb::color(plotnode.get("LColorPull","azure+3")));
  hpull->SetDrawOption (plotnode.get("OptionPull","e1p").data());

  // Create a new frame to draw the pull distribution and add the distribution to the frame
  RooPlot* frame2 = Observable->frame(plotnode.get<double>("low"),
                                      plotnode.get<double>("high"),
                                      plotnode.get<int>   ("bins"));
  frame2->SetTitle(" ");
  frame2->GetXaxis()->SetTitleOffset(plotnode.get("xOffset",1.05));
  frame2->GetXaxis()->SetTitleSize  (textsize/padsplit);
  frame2->GetXaxis()->SetLabelSize  (textsize/padsplit);
  frame2->GetXaxis()->SetTitle      (plotnode.get("xtitle","M").data());
  frame2->GetXaxis()->SetTickLength (frame->GetXaxis()->GetTickLength()/(padsplit/(1-padsplit)));
  frame2->GetYaxis()->SetLabelSize  (textsize/padsplit);
  frame2->GetYaxis()->SetTitleSize  (textsize/padsplit);
  frame2->GetYaxis()->SetNdivisions (plotnode.get("xdiv",5));
  frame2->GetYaxis()->SetTitleOffset(plotnode.get("yOffset",1.2)*padsplit);
  frame2->GetYaxis()->SetTitle      (plotnode.get("ytitlePull","Pull ").data());
  frame2->GetYaxis()->SetRangeUser  (-plotnode.get("PullRange",4.8),
                                     plotnode.get("PullRange",4.8));
  frame2->addPlotable(hpull,plotnode.get("OptionPull","e1p").data());

  std::vector< std::unique_ptr<TLine> > plines;
  if(plotnode.get_child_optional("pullLines")){
    for(const auto& l : plotnode.get_child("pullLines")) {
      std::unique_ptr<TLine> pullline( new TLine(plotnode.get<double>("low"),l.second.get<double>("pull"),
                                                 plotnode.get<double>("high"),l.second.get<double>("pull")));
      pullline->SetLineStyle(l.second.get("style",2));
      pullline->SetLineWidth(l.second.get("width",1));
      pullline->SetLineColor(LHCb::color(l.second.get("color","black")));
      pullline->SetDrawOption(l.second.get("option","l").data());
      plines.push_back(std::move(pullline));
    }
  }
  for(const auto& line : plines){
    frame2->addObject(line.get());
  }

  TPaveText HideExpXP2(1.002-pad2.GetRightMargin(),pad2.GetBottomMargin(),1,pad2.GetBottomMargin()+0.1,"BRNDC");
  HideExpXP2.SetBorderSize(0);HideExpXP2.SetFillColor(kWhite);HideExpXP2.SetFillStyle(1001);HideExpXP2.SetTextColor(kWhite);
  HideExpXP2.AddText(" ");
  if(plotnode.get_optional<bool>("HideExpX"))
    frame2->addObject(&HideExpXP2);

  pad2.Draw();
  pad2.cd();
  frame2->Draw();
  frame2->SetName("pulls");
  c1.Update();
  frame2->Write();
  c1.Write();

  //Small convergence check
  auto bins_gt_threshold = 0;
  //pulls should be within "pull_threshold" sigma
  auto pull_threshold = 4;
  //number of bins that are allowed to be bigger than pull_threshold
  auto n_bins_gt_pull_threshold = 2;
  const auto pulls = hpull->GetY();
  for(auto ij = 0; ij < plotnode.get<double>("bins"); ij++)
    if(fabs(pulls[ij]) > pull_threshold)bins_gt_threshold++;
  if(bins_gt_threshold >= n_bins_gt_pull_threshold)
    msgsvc.warningmsg("\033[0;31mPulls too high! Please have a look\033[0m");

  if(gErrorIgnoreLevel == kWarning && msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    gErrorIgnoreLevel = kInfo;//i still want to know where the plots have been saved

  TString fname = outfile->GetName();fname.Remove(fname.Sizeof()-6,6);//remove ".root"
  //strip prepended path and add observable name
  auto autogen_plotname = static_cast<std::string>(TString::Format("%s_%s",static_cast<TObjString*>(fname.Tokenize("/")->Last())->String().Data(),Observable->GetName()).Data());
  auto plotname = prefix;
  plotname.append(plotnode.get("plotname",autogen_plotname));
  
  //check if plotname contains a directory and if that exists (boost::filesystem would be nice)
  TString cpn = plotname;
  auto oasize = cpn.Tokenize("/")->GetEntries();
  if(oasize > 0){
    TString tmpdir;
    auto oa = cpn.Tokenize("/");
    for(decltype(oasize) oai = 0; oai < oasize-1; oai++ )
      tmpdir += static_cast<TObjString*>(oa->At(oai))->String() + "/";
    if(!gSystem->OpenDirectory((workdir + "/" + static_cast<std::string>(tmpdir)).data()))
      gSystem->mkdir((workdir + "/" + static_cast<std::string>(tmpdir)).data());
  }
  for(const auto& ex : plotnode.get_child("outputformats"))
    c1.SaveAs((workdir+"/"+plotname+"."+ex.first).data());

  return;
}
